﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//data structure that describes a pre-defined wave of enemies
//can be saved in the assets folder
//has a round time which is needed for game state
//consists of several groups with associated timings
//location of spawn is defined by spawn manager
[CreateAssetMenu(fileName = "Wave", menuName = "Custom/Wave", order = 1)]
public class Wave : ScriptableObject
{
    public float WaveTime;
    public List<Group> groups = new List<Group>();

    [System.Serializable]
    public class Group
    {
        public float spawnTime;
        public GameObject[] enemies;
    }
}
