﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class EnemyManager : NetworkBehaviour
{
    public Wave[] waves;
    public EnemySpawner[] spawnpoints;

    private Wave.Group[] currentWave;
    
    public GameObject[] playerBase;
    private static GameObject[] playerBases;

    public static Transform[] basePos;

    public static List<GameObject> enemyList = new List<GameObject>();

    private List<IEnumerator> coroutine;

    // Does not have to be reset because these values are never changed
    void Awake()
    {
        coroutine = new List<IEnumerator>();
        basePos = new Transform[playerBase.Length];
        playerBases = playerBase;
        for (int i = 0; i < playerBase.Length; i++)
        {
            basePos[i] = playerBase[i].transform;
        }
    }

    private void Update()
    {
       // if (Input.GetKeyDown(KeyCode.P))
       // {
       //     resetEnemies();
       // }
    }
    public static GameObject[] getPlayerBases()
    {
        return playerBases;
    }

    // Resets all the enemies and the spawnPoints
    public void resetEnemies()
    {
        // resets the spawnpoints
        for (int i = 0; i < spawnpoints.Length; i++)
        {
            spawnpoints[i].resetSpawner();
        }
        // removes all the enemies in the scene
        foreach (GameObject enemy in enemyList)
        {
            Destroy(enemy);
        }
        enemyList = new List<GameObject>();

        foreach (IEnumerator enumerator in coroutine)
        {
                if(!(enumerator == null))
                {
                    StopCoroutine(enumerator);
                }
            
        }
    }

    public override void OnStartServer()
    {
        //initialize time on server
       // spawnWave(0);
    }

    public void spawnWave(int numb)
    {
        if (numb > waves.Length || !isServer)
        {
            if (numb > waves.Length)
            {
                Debug.Log("Error there is no wave number " + numb + ". Please not that the number does not indicate the name but the position in the array.");
            }
            return;
        }
        IEnumerator spawnGroupR;
        foreach (Wave.Group element in waves[numb].groups)
        {
            spawnGroupR = spawnGroup(element);
            coroutine.Add(spawnGroupR);
            StartCoroutine(spawnGroupR);
        }
    }
    IEnumerator spawnGroup(Wave.Group element)
    {
        yield return new WaitForSeconds(element.spawnTime);
        lock(spawnpoints)
        {
            EnemySpawner[] freeSpawner = freeSpawners();
            if (freeSpawner.Length == 0)
            {
                Debug.Log("All spawners are on cooldown");
            }
            else
            {
             int r = Random.Range(0, freeSpawner.Length);
                 freeSpawner[r].spawnGroup(element);
            }
        }
    }
    // returns an EnemySpawner array with the spawners which are not on cooldown
    private EnemySpawner[] freeSpawners()
    {
        int notOnC = 0;
        for (int i = 0; i < spawnpoints.Length; i++)
        {
            if (!spawnpoints[i].isOnCooldown())
            {
                notOnC++;
            }
        }
        EnemySpawner[] eS = new EnemySpawner[notOnC];
        int poscount = 0;
        for (int i = 0; i < spawnpoints.Length;i++)
        {
            if (!spawnpoints[i].isOnCooldown())
            {
                eS[poscount] = spawnpoints[i];
                poscount++;
            }
        }
        return eS;
    }

    public Wave GetWave(int index)
    {
        if (index < 0 || index >= waves.Length)
        {
            Debug.Log("no Wave with index " + index);
            return null;
        }

        return waves[index];
    }

    public int NumberOfWaves
    {
        get { return waves.Length; }
    }
}
