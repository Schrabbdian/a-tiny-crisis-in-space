﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class RobucketAI : NetworkBehaviour
{
    public GameObject spawnParticle;
    //Component References
    private NavMeshAgent agent;

    //Attack
    [SerializeField] private Transform MissileSpawn;
    [SerializeField] private GameObject homingMissilePrefab;
    [SerializeField] private int MissilesPerAttack;
    [SerializeField] private float ShootMissilesRange;
    [SerializeField] private float MaxCooldown;
    private float lastRockets;

    //Movement Parameters
    public float AggroDistance = 2;
    // Relative to the players
    public float DeAggroDistance = 3;
    public float AttackDistance = 0.5f;
    public float goStraightToBaseDist = 4;

    //Target / Pathfinding
    // The player in relevant range to the enemy
    private List<PlayerIngameInfo> players;
    // The Distance when the player came into the enemies range

    // The targeted player. Should be null if the enemy is currently targeting a base
    private bool targetIsPlayer = false;
    private GameObject target;

    private bool dead;
    private float startSpeed;
    [SerializeField] private GameObject deathParticle;

    //animation
    private Animator animator;

    // Use this for initialization
    public void Start() {
      
            Instantiate(spawnParticle, transform.position + new Vector3(0, -0.6f, 0), transform.rotation);
       

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = GetComponent<Animator>();
        lastRockets = -MaxCooldown;

	    if (!isServer)
	    {
	        // Disables the agent on the client
	        agent.enabled = false;
	        
	    }
	    else
	    {
	        startSpeed = agent.speed;
	    }
	}
	
	// Update is called once per frame
	void Update () {
        if(!isServer)
            return;

	    goToClosestTarget();

	    if (canShootTarget())
	    {
	        if (Time.time >= lastRockets + MaxCooldown)
	        {
	            StartCoroutine(ShootDelayed(0.5f));
	        }
	    }

	    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Robucket Shoot"))
	    {
	        animator.SetBool("Shoot", false);
	        agent.speed = 0;
        }
	    else
	    {
	        agent.speed = startSpeed;
	    }

        animator.SetFloat("Speed", Mathf.Clamp01(agent.velocity.magnitude / startSpeed));
	}

    // The agent moves to the closest target. A target which has been chased befor will be prefared as well as bases if they are very close
    private void goToClosestTarget()
    {
        players = ((CustomLobbyManager)NetworkManager.singleton).GetServerSidePlayerInfos();
        // If the current target is a player then the enemy should keep chasing him until someone gets close to him respektivly
        if (targetIsPlayer)
        {
            //  The current distance to the current target. If there is none the dist is max float
            float currentDist;
            // the distance in which the enemy will chose a player as target
            float attackdist;
            // normal case
            if (target != null && !(target.GetComponent<PlayerIngameInfo>().Health.GetCurrentHealth() <= 0))
            {
                currentDist = getDistance(target.transform.position) * 2 / 3;
                attackdist = DeAggroDistance;
                target = null;
            }
            else// player has died or merged
            {
                currentDist = float.MaxValue;
                attackdist = AggroDistance;
            }

            foreach (PlayerIngameInfo p in players)
            {
                if (p.PlayerObject == null)
                    continue;

                float distance = getDistance(p.PlayerObject.transform.position);
                // For a target to be selected it has to be coloser than the current target by 2/3 or any value closer to a former chosen closer target. Also it has to be in the attack range
                if (distance < currentDist && distance < attackdist && !(p.Health.GetCurrentHealth() <= 0))
                {
                    attackdist = AggroDistance;
                    target = p.PlayerObject;
                    currentDist = distance;
                }
            }
            // If no player has meet the critiria so far the colosest base should be selected no matter how far it is. 
            if (target == null)
            {
                currentDist = float.MaxValue;
            }

            foreach (Transform g in EnemyManager.basePos)
            {
                if (g != null)
                {
                    float distance = getDistance(g.transform.position);
                    if (distance < currentDist || distance < goStraightToBaseDist)
                    {
                        targetIsPlayer = false;
                        target = g.gameObject;
                        currentDist = distance;
                    }
                }
            }
        }
        // The last target was a base
        else
        {
            float currentDist = float.MaxValue;
            target = null;
            foreach (PlayerIngameInfo p in players)
            {
                if (p.PlayerObject == null)
                    continue;

                float distance = getDistance(p.PlayerObject.transform.position);
                // For a target to be selected it has to be coloser than the current target by 2/3 or any value closer to a former chosen closer target. Also it has to be in the attack range
                if (distance < currentDist && distance < AggroDistance && !(p.Health.GetCurrentHealth() <= 0))
                {
                    target = p.PlayerObject;
                    currentDist = distance;
                }
            }
            // If no player has met the criteria so far the closest base should be selected no matter how far it is. 
            if (target == null)
            {
                currentDist = float.MaxValue;
            }

            foreach (Transform g in EnemyManager.basePos)
            {
                if (g != null)
                {
                    float distance = getDistance(g.transform.position);
                    if (distance < currentDist || distance < goStraightToBaseDist)
                    {
                        targetIsPlayer = false;
                        target = g.gameObject;
                        currentDist = distance;
                    }
                }
            }
        }
        // Should actually never happen but we do not want anything to crash
        if (target == null)
        {
            target = this.gameObject;
        }
        agent.SetDestination(target.transform.position);
    }
    // Gives the length of the Path from the agent to the goal. This is not the direct way but the way a nav mesh agent would take
    private float getDistance(Vector3 goal)
    {
        return Vector3.Distance(goal, transform.position);
        /*UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        if (agent.CalculatePath(goal, path))
        {
            Vector3[] corners = path.corners;
            float length = 0;
            for (int i = 0; i < corners.Length - 1; i++)
            {
                length += Vector3.Distance(corners[i], corners[i + 1]);
            }
            return length;
        }
        else
        {
            // if a target can not be reached the method return the highest possible number
            return float.MaxValue;
        }*/

    }

    private bool canShootTarget()
    {
        return Vector3.Distance(target.transform.position, transform.position) <= ShootMissilesRange;
    }

    private IEnumerator ShootDelayed(float delay)
    {
        animator.SetBool("Shoot", true);

        lastRockets = Time.time;

        yield return new WaitForSeconds(delay);

        SpawnRockets();
    }

    private void SpawnRockets()
    {
        for (int i = 0; i < MissilesPerAttack; i++)
        {
           Vector3 spawnPos = MissileSpawn.transform.position + Random.insideUnitSphere * 1.6f;

            Vector2 dirOffset = Random.insideUnitCircle;
            Vector3 spawnDir = new Vector3(dirOffset.x, 1, dirOffset.y);
            Quaternion lookRotation = Quaternion.LookRotation(spawnDir);

            GameObject missile = Instantiate(homingMissilePrefab, spawnPos, lookRotation);

            //set target
            missile.GetComponent<HomingMissile>().target = target;

            NetworkServer.Spawn(missile); 
        }
    }

    //as soon as a bomb enters the robucket's bucket it will die
    //we deactivate the bomb so it doesnt deal damage to surrounding enemies
    void OnTriggerEnter(Collider coll)
    {
        if(!isServer)
            return;

        Bomb bomb = coll.gameObject.GetComponent<Bomb>();
        if (!dead && bomb)
        {
            dead = true;

            bomb.Deactivate();

            StartCoroutine(InitiateDeath(bomb.timeToLive));
        }
    }

    private IEnumerator InitiateDeath(float bombTimeToLive)
    {
        yield return new WaitForSeconds(bombTimeToLive);
        RpcDeathAnimation();
        //yield return new WaitForSeconds(0.02f);
        //NetworkServer.Destroy(gameObject);
    }
    [ClientRpc]
    public void RpcDeathAnimation()
    {
        StartCoroutine(Dieing());
    }
    IEnumerator Dieing()
    {
        //this.GetComponent<EnemyAIScript>().enabled = false;
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<Animator>().enabled = false;
        for (int i = 0; i < 20; i++)
        {
            this.transform.localScale *= 0.8f;
            yield return new WaitForSeconds(0.02f);
        }

        Instantiate(deathParticle, transform.position + new Vector3(0, 0, 0), transform.rotation);
        Destroy(gameObject, 0.1f);
        Debug.Log(gameObject.name + " dead");
    }
}
