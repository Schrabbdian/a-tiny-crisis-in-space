﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class EnemyAIScript : NetworkBehaviour {
    [SerializeField] private GameObject deathParticle;
    public GameObject spawnParticle;

    public Collider head;

    public float damage = 1;
    public float AggroDistance=2;
    // Relative to the players
    public float DeAggroDistance=3;
    public float AttackDistance = 0.5f;
    public float goStraightToBaseDist = 4;
    
    // If this bool is taged the enemies will not move while attacking
    public bool stopmoving = false;
        // Is used to stop the enemeies from moving
        private float speed = 0;


    private float attackCoolDown = 0;
    private float maxAttackCoolDown = 1;
    
    // The player in relevant range to the enemy
    private List<PlayerIngameInfo> players;
    // The Distance when the player came into the enemies range

    // The targeted player. Should be null if the enemy is currently targeting a base
    private bool targetIsPlayer = false;
    private GameObject target;
    
    private UnityEngine.AI.NavMeshAgent agent;

    // The cordinates of the closestBase this value is updated when the enemy changes target
    private Vector3 closestBaseOb;
    void Start () {
       
            Instantiate(spawnParticle,transform.position+new Vector3(0,-0.6f,0),transform.rotation);
       
        
        if (!isServer)
        {
            // Disables the agent on the client
            agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            agent.enabled = false;
            return;
        }

        if (isServer)
        {
            agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            // As the activ part of the animation 14% long if the cooldown is 25% of this there should not be any problems
            speed = agent.speed;
            goToClosestTarget();
            targetIsPlayer = false;
        }
    }
    // sets the path to the closest target regarding some rules
   public void Update()
    {
        if (!isServer)
        {
            return;
        }
        attackCoolDown -= Time.deltaTime;
        goToClosestTarget();
        checkForAttack();
    }
    // The agent moves to the closest target. A target which has been chased befor will be prefared as well as bases if they are very close
    private void goToClosestTarget()
    {
        players = ((CustomLobbyManager) NetworkManager.singleton).GetServerSidePlayerInfos();
        // If the current target is a player then the enemy should keep chasing him until someone gets close to him respektivly
        if (targetIsPlayer)
        {
            //  The current distance to the current target. If there is none the dist is max float
            float currentDist;
            // the distance in which the enemy will chose a player as target
            float attackdist;
            // normal case
            if (target != null && !(target.GetComponent<PlayerIngameInfo>().Health.GetCurrentHealth() <= 0))
            {
                currentDist = getDistance(target.transform.position)*2/3;
                attackdist = DeAggroDistance;
                target = null;
            }
            else// player has died or merged
            {
                currentDist = float.MaxValue;
                attackdist = AggroDistance;
            }

            foreach (PlayerIngameInfo p in players)
            {
                if(p.PlayerObject == null)
                    continue;

                float distance = getDistance(p.PlayerObject.transform.position); 
                // For a target to be selected it has to be coloser than the current target by 2/3 or any value closer to a former chosen closer target. Also it has to be in the attack range
                if (distance < currentDist && distance < attackdist && !(p.Health.GetCurrentHealth() <= 0))
                {
                    attackdist = AggroDistance;
                    target = p.PlayerObject;
                    currentDist = distance;
                }
            }
            // If no player has meet the critiria so far the colosest base should be selected no matter how far it is. 
            if (target == null)
            {
                currentDist = float.MaxValue;
            }

            foreach (Transform g in EnemyManager.basePos)
            {
                if (g != null)
                {
                    float distance = getDistance(g.transform.position); 
                    if (distance < currentDist || distance < goStraightToBaseDist)
                    {
                        targetIsPlayer = false;
                        target = g.gameObject;
                        currentDist = distance;
                    }
                }
            }
        }
        // The last target was a base
        else
        {
            float currentDist = float.MaxValue;
            target = null;
            foreach (PlayerIngameInfo p in players)
            {
                if (p.PlayerObject == null)
                    continue;

                float distance = getDistance(p.PlayerObject.transform.position); 
                // For a target to be selected it has to be coloser than the current target by 2/3 or any value closer to a former chosen closer target. Also it has to be in the attack range
                if (distance < currentDist && distance < AggroDistance && !(p.Health.GetCurrentHealth() <= 0))
                {
                    target = p.PlayerObject;
                    currentDist = distance;
                }
            }
            // If no player has meet the critiria so far the colosest base should be selected no matter how far it is. 
            if (target == null)
            {
                currentDist = float.MaxValue;
            }

            foreach (Transform g in EnemyManager.basePos)
            {
                if (g != null)
                {
                    float distance = getDistance(g.transform.position);
                    if (distance < currentDist || distance < goStraightToBaseDist)
                    {
                        targetIsPlayer = false;
                        target = g.gameObject;
                        currentDist = distance;
                    }
                }
            }
        }
        // Should actually never happen but we do not want anything to crash
        if (target == null )
        {
            target = this.gameObject;
        }
        agent.SetDestination(target.transform.position);
    }
    // Gives the length of the Path from the agent to the goal. This is not the direct way but the way a nav mesh agent would take
    private float getDistance(Vector3 goal)
    {
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        if (agent.CalculatePath(goal, path))
        {
            Vector3[] corners= path.corners;
            float length = 0;
            for (int i = 0; i < corners.Length - 1; i++)
            {
                length += Vector3.Distance(corners[i], corners[i + 1]);
            }
            return length;
        }
        else
        {
            // if a target can not be reached the method return the highest possible number
            return float.MaxValue;
        }

    }

    //-------------------------   currently not used
    //returns the highest number in a vec3
    private float maxVec(Vector3 vec)
    {
        if (vec.x >= vec.y && vec.x >= vec.z)
        {
            return vec.x;
        }
        if (vec.y >= vec.z)
        {
            return vec.y;
        }
        return vec.z;
    }
    // returns the Closest base

    /*private Transform closestBase()
    {
        float lng = 0;
        Transform small = null;
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        float lastDistance = float.MaxValue;
        for(int i=0;i<EnemyManager.basePos.Length;i++)
        {
            agent.CalculatePath(EnemyManager.basePos[i].position,path);
            lng = 0;
            // Calculates the length of the path
            for (int j = 1; j < path.corners.Length; ++j)
            {
                lng += Vector3.Distance(path.corners[j - 1], path.corners[j]);
            }
            if (lastDistance > lng)
            {
                small = EnemyManager.basePos[i];
                lastDistance = lng;
            }
        }
        return small;
    }/*/
    //----------------- currently not used

    //--------------------------------------------------------------
    // Attacking
    // Checks if the enemie is close enoth to a target to attack and if so plays the animations
    private void checkForAttack()
    {
        // The target is either a player or a base
        Vector3 targetPos = target.transform.position;
        
        if (Vector3.Distance(targetPos, this.transform.position) < AttackDistance + getRadius(target))
        {
            GetComponent<Animator>().SetBool("Attacking", true);
            if (stopmoving)
            {
                agent.speed = 0;
            }
        }

        else
        {
            GetComponent<Animator>().SetBool("Attacking", false);
            if (stopmoving)
            {
                agent.speed = speed;
            }
        }
    }
    
    // Gets the distance to the center depending on the kinde of collider
    private float getRadius(GameObject target)
    {

        if (target == null)
        {
            return 0;
        }

        float scaleMax = 0;
        if (target.transform.lossyScale.x >= target.transform.lossyScale.z)
        {
            scaleMax = target.transform.lossyScale.x;
        }
        else
        {
            scaleMax = target.transform.lossyScale.z;
        }
        Collider col = target.GetComponent<Collider>();
        if (col is SphereCollider)
        {
            return ((SphereCollider)col).radius * scaleMax;
        }

        if (col is CapsuleCollider)
        {
            return ((CapsuleCollider)col).radius * scaleMax;
        }
        if (col is BoxCollider)
        {
            if (((BoxCollider)col).size.x >= ((BoxCollider)col).size.z)
            {
                return ((BoxCollider)col).size.x * scaleMax;
            }
            else
            {
                return ((BoxCollider)col).size.y * scaleMax;
            }
        }
        return 0;
    }
    

    // Basiclly the ontrifferEnter of the enemy head if a player or the base was found
    public void headHasFoundTarget(Collider other)
    {
        if (GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.name == "BasicRobot_Bite" && attackCoolDown < 0)
        {
            // gives the relative time point in the animation 
            float pointInAnimation = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime;
            pointInAnimation = pointInAnimation - (float)(int)pointInAnimation;
            if (pointInAnimation > 0.38f && pointInAnimation < 0.52f)
            {
                maxAttackCoolDown = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length / GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed;
                attackCoolDown = maxAttackCoolDown;
                other.GetComponent<Health>().TakeDamage(damage, Health.DamageType.Enemy);
            }
        }
    }

}