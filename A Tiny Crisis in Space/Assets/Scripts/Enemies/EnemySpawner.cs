﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class EnemySpawner : NetworkBehaviour {
    // The time between the spawning of two enemies
    public int spawnerID = 0; // Value to debug
    [SerializeField] private float cooldownMax = 10.0f;
    private float cooldown = 0;
    // time between two enemies.
    [SerializeField] private float TimeBetween = 1;
    private GameObject[] enemies;
    // counts how many enemies of the current wave have allready been spawned
    private int eCounter = 0;
    private float time = 1;

    public void spawnGroup(Wave.Group group)
    {
        enemies = group.enemies;
        cooldown = cooldownMax;
    }

    private void spawnEnemy(GameObject enemy)
    {
        enemy = Instantiate(enemy, this.transform.position, this.transform.rotation);
        enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        enemy.transform.position = this.transform.position;
        enemy.transform.rotation = this.transform.rotation;
        enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        EnemyManager.enemyList.Add(enemy);
        NetworkServer.Spawn(enemy);
    }

    public void resetSpawner()
    {
        cooldown = 0;
        enemies = null;
        eCounter = 0;
        time = 1;
    }
    public void Update()
    {
        if (!isServer)
        {
            return;
        }
        cooldown -= Time.deltaTime;
        time += Time.deltaTime;
        if(enemies != null && time>=TimeBetween)
        {
            time = 0;
            spawnEnemy(enemies[eCounter]);
            eCounter++;
            if (eCounter >= enemies.Length)
            {
                enemies = null;
                eCounter = 0;
            }
        }
    }
    public bool isOnCooldown()
    {
        if (cooldown > 0 || enemies != null)
        {
            return true;
        }
        return false;
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawIcon(transform.position, "skull.png", true);
    }
}




