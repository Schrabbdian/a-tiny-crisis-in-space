﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyHeadColliderCheck : MonoBehaviour {
    public EnemyAIScript mainEnemy;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        BaseHealth h= other.GetComponent<BaseHealth>();
        if (h!=null|| other.tag == "Player")
        {
            mainEnemy.headHasFoundTarget(other);
        }
    }
}
