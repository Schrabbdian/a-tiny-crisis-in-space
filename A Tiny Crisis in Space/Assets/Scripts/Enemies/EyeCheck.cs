﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Checks if the Eye of the Orbbot has been hit
public class EyeCheck : MonoBehaviour {
    public GameObject Roball;
    // Saves if the button is the front button
    public SkinnedMeshRenderer rend;
    public bool front;
    private float cooldown=0;
    private float maxCD = 0.5f;
    private Color baseColor;
    [SerializeField] private Color col;
    private RoballAI AI;

    private IEnumerator lastBlink;

    private void Start()
    {
        AI = Roball.GetComponent<RoballAI>();

        int x;
        if (front)
        {
            x = 1;
        }
        else
        {
            x = 2;
        }
        baseColor = rend.materials[x].color;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag.Equals("Projectile"))
        {
            if (cooldown >= maxCD)
            {
                if (lastBlink != null)
                {
                    StopCoroutine(lastBlink);
                }
                lastBlink = Blink();
                StartCoroutine(lastBlink);

                AI.buttonPressed(front,maxCD);
                cooldown = 0;
            }
        }
    }

    private void Update()
    {
        if (cooldown < maxCD)
        {
            cooldown += Time.deltaTime;
        }
        if (cooldown >= maxCD && cooldown < maxCD * 3)
        {
            cooldown *= 3;
            AI.buttonNotPressed(front);
        }
    }

    IEnumerator Blink()
    {
        int x;
        if (front)
        {
            x = 1;
        }
        else
        {
            x = 2;
        }
        Color change = (baseColor - col) / (maxCD*50) * 2;
        for (int i = 0; i < maxCD*50; i++)
        {
            if (i < (maxCD *50)/ 2)
            {
                rend.materials[x].color -= change;
            }
            else
            {
                rend.materials[x].color += change;
            }
            
            yield return null;
        }
        rend.materials[x].color = baseColor;
    }
}
