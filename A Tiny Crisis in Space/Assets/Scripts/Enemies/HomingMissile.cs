﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HomingMissile : NetworkBehaviour
{
    //prefabs
    public GameObject explosionEffect;

    public GameObject target;

    //Damage
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float Damage;
    [SerializeField] private float DamageRadius;
    //[SerializeField] private float DamageForce;

    //Movement
    [SerializeField]
    private float MaxSpeed;
    [SerializeField] private float TurnSpeed = 10;
    
    //component references
    private Rigidbody rigidBody;

    //private stuff
    private Vector3 lastTargetPosition;
    private bool hasHitSomething = false;

	// Use this for initialization
	void Start ()
	{
	    rigidBody = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void FixedUpdate () {
        if(!isServer)
            return;

		RotateTowardsTarget();

	    rigidBody.velocity = transform.forward * MaxSpeed;
	}

    private void RotateTowardsTarget()
    {
        Vector3 toTarget = targetPosition() - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(toTarget.normalized);

        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, TurnSpeed * Time.fixedDeltaTime);
    }


    private Vector3 targetPosition()
    {
        if (target)
        {
            lastTargetPosition = target.transform.position;
        }

        return lastTargetPosition;
    }

    void OnCollisionEnter(Collision coll)
    {

        if (hasHitSomething)
            return;

        hasHitSomething = true;
        
        Instantiate(explosionEffect, transform.position, Quaternion.identity);

        GetComponentInChildren<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        if (isServer)
        {
            Explode();
        }
        else
        {
        }
    }

    private void Explode()
    {
        Collider[] overlaps = Physics.OverlapSphere(transform.position, DamageRadius, layerMask);
        foreach (Collider collider in overlaps)
        {
            Rigidbody r = collider.gameObject.GetComponent<Rigidbody>();
            if (r)
            {
                //todo add explosion force as knockback to player
                //r.AddExplosionForce(DamageForce, transform.position, DamageRadius);
            }

            Health h = collider.gameObject.GetComponent<Health>();
            if (h)
            {
                h.TakeDamage(Damage, Health.DamageType.Enemy);
            }
        }

        Destroy(gameObject, 1f);
    }
}
