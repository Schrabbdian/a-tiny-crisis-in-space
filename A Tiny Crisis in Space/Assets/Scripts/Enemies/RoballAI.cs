﻿using System;
using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class RoballAI : NetworkBehaviour
{
    [SerializeField] private GameObject deathParticle;
    public GameObject spawnParticle;
    public GameObject frontButton;
    public GameObject backButton;

    private AIState state;
    private enum AIState
    {
        Walk, TrackPlayer, ShootLaser, Dead
    }
    //GameObject References
    [SerializeField] private Transform LaserPivot;
    [SerializeField] private Transform LaserStart;

    //Component References
    private NavMeshAgent agent;
    private Animator animator;
    private LineRenderer laserRenderer;
   

    //Attack
    [SerializeField] private float laserDamge = 10;
    [SerializeField] private float ShootLaserRange;
    [SerializeField] private float MaxCooldown = 5;
    [SerializeField] private LayerMask laserLayerMask;
    [SerializeField] private float TrackingDuration = 1f;
    [SerializeField] private Color TrackingColor;
    [SerializeField] private float LaserDuration = 0.4f;
    [SerializeField] private Color LaserColor;
    [SerializeField] private float aimTurnSpeed = 7.5f;
    private float lastLaser;
    private float lastTrackingStart;
    private Vector3 targedPos;
    private Vector3 laserEndPosition;


    // Life (Stuff so that the Enemy can be killed)
    private bool[] buttonsPressed;
    private int numbButtons = 2;


    // Speed
    private Vector3 lastPos;
    private float speed;

    //Movement Parameters
    public float AggroDistance = 2;
    // Relative to the players
    public float DeAggroDistance = 3;

    public float goStraightToBaseDist;

    //Pathfinding
    public GameObject target;
    private float startSpeed;
    private List<PlayerIngameInfo> players;
    private bool targetIsPlayer;

    // Use this for initialization
    public void Start()
    {
        // Spawns the particle effect
        Instantiate(spawnParticle, transform.position + new Vector3(0, -0.6f, 0), transform.rotation);

        // Initalizas the buttons
        buttonsPressed = new bool[numbButtons];
        for (int i = 0; i < numbButtons; i++)
        {
            buttonsPressed[i] = false;
        }
        // Saves values for simpler use
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = GetComponent<Animator>();

        // Initalize Laser
        laserRenderer = LaserStart.GetComponent<LineRenderer>();
        lastLaser = -MaxCooldown;

        laserRenderer.enabled = false;

        if (!isServer)
        {
            // Disables the agent on the client
            agent.enabled = false;
        }
        else
        {
            startSpeed = agent.speed;
            animator.SetFloat("Speed", Vector3.Magnitude(GetComponent<Rigidbody>().velocity));
            animator.SetBool("FrontShot", false);
            animator.SetBool("BackShot", false);
            animator.SetBool("Dead", false);
            animator.SetBool("Shooting",false);

            goToClosestTarget();
            targetIsPlayer = false;
            lastPos = transform.position;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }
        
    }


    // Update is called once per frame
    void Update()
    {
        if (!isServer)
            return;

        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Fire"))
        {
            animator.SetBool("Shooting",false);
        }

        // Checks if both buttons have been shot and so the Enemy should die
        if (buttonsPressed[0] && buttonsPressed[1] && state!=AIState.Dead)
        {
            dieing();
        }
        speed= Vector3.Magnitude(transform.position - lastPos)*100;
        lastPos = transform.position;
        switch (state)
        {
                case AIState.Walk:
                    agent.speed = startSpeed;
                    animator.SetFloat("Speed", speed);
                    goToClosestTarget();

                    if (canSeeTarget() && targetInRange() && laserOffCooldown())
                    {
                        state = AIState.TrackPlayer;
                        lastTrackingStart = Time.time;
                        RpcSetLaser(Vector3.zero, true, Color.cyan);
                    }
                    
                break;

                case AIState.TrackPlayer:
                    agent.speed = 0;
                    animator.SetFloat("Speed", 0);
                    animator.SetBool("Charging",true);
                    

                    targedPos = TrackTarget();
                    laserEndPosition = getNextGroundPoint(targedPos,LaserStart.position);
                    RpcSetLaser(laserEndPosition, true, TrackingColor);

                    //track the player for a certain amount of time
                    if (Time.time >= lastTrackingStart + TrackingDuration)
                    {
                         animator.SetBool("Charging", false);
                         state = AIState.ShootLaser;

                        RpcSetLaser(laserEndPosition, true, LaserColor);
                    }
                    // TODO if the player moves out of range or we cant see him anymore
                    // -> go back to walking state
                break;

                case AIState.ShootLaser:
                    agent.speed = 0;
                    animator.SetFloat("Speed", speed);
                    animator.SetBool("Shooting",true);
                    
                if (laserOffCooldown())
                        ShootLaser();

                    if (Time.time >= lastLaser + LaserDuration)
                    {
                    // deal damage to player if hit
                    RaycastHit[] hits = getSortedRaycasthits(LaserStart.position, laserEndPosition - LaserStart.position);
                    for (int i = 0; i < hits.Length; i++)
                    {
                        if (hits[i].collider.tag.Equals("Untagged")|| hits[i].distance > ShootLaserRange)
                        {
                            if (hits[i].collider.tag.Equals("Untagged"))
                                {
                                BaseHealth h = hits[i].collider.GetComponent<BaseHealth>();
                                if (h != null)
                                {
                                    h.TakeDamage(laserDamge, Health.DamageType.Enemy);
                                }
                            }

                            i = hits.Length+1;
                        }
                        else if (hits[i].collider.tag.Equals("Player"))
                        {
                        hits[i].collider.GetComponent<Health>().TakeDamage(laserDamge,Health.DamageType.Enemy);
                        }
                        
                    }
                    // Change laser color
                    RpcSetLaser(Vector3.zero, false, Color.white);
                    // start walking again
                        state = AIState.Walk;

                    animator.SetBool("Shooting", false);
                }
                break;
            case AIState.Dead:

                break;
        }
    }

    private void OnDestroy()
    {
        Instantiate(deathParticle, transform.position + new Vector3(0, 0, 0), transform.rotation);
    }

    // The agent moves to the closest target. A target which has been chased befor will be prefared as well as bases if they are very close
    private void goToClosestTarget()
    {
        players = ((CustomLobbyManager)NetworkManager.singleton).GetServerSidePlayerInfos();
        // If the current target is a player then the enemy should keep chasing him until someone gets close to him respektivly
        if (targetIsPlayer)
        {
            //  The current distance to the current target. If there is none the dist is max float
            float currentDist;
            // the distance in which the enemy will chose a player as target
            float attackdist;
            // normal case
            if (target != null && !(target.GetComponent<PlayerIngameInfo>().Health.GetCurrentHealth() <= 0))
            {
                currentDist = getDistance(target.transform.position,target) * 2 / 3;
                attackdist = DeAggroDistance;
                target = null;
            }
            else// player has died or merged
            {
                currentDist = float.MaxValue;
                attackdist = AggroDistance;
            }

            foreach (PlayerIngameInfo p in players)
            {
                if (p.PlayerObject == null)
                    continue;

                float distance = getDistance(p.PlayerObject.transform.position,p.PlayerObject.gameObject);
                // For a target to be selected it has to be coloser than the current target by 2/3 or any value closer to a former chosen closer target. Also it has to be in the attack range
                if (distance < currentDist && distance < attackdist && !p.Health.IsDead())
                {
                    attackdist = AggroDistance;
                    target = p.PlayerObject;
                    currentDist = distance;
                }
            }
            // If no player has meet the critiria so far the colosest base should be selected no matter how far it is. 
            if (target == null)
            {
                currentDist = float.MaxValue;
            }

            foreach (Transform g in EnemyManager.basePos)
            {
                if (g != null)
                {
                    float distance = getDistance(g.transform.position,g.gameObject);
                    if (distance < currentDist || distance < goStraightToBaseDist)
                    {
                        targetIsPlayer = false;
                        target = g.gameObject;
                        currentDist = distance;
                    }
                }
            }
        }
        // The last target was a base
        else
        {
            float currentDist = float.MaxValue;
            target = null;
            foreach (PlayerIngameInfo p in players)
            {
                if (p.PlayerObject == null)
                    continue;

                float distance = getDistance(p.PlayerObject.transform.position,p.PlayerObject.gameObject);
                // For a target to be selected it has to be coloser than the current target by 2/3 or any value closer to a former chosen closer target. Also it has to be in the attack range
                if (distance < currentDist && distance < AggroDistance && !(p.Health.GetCurrentHealth() <= 0))
                {
                    target = p.PlayerObject;
                    currentDist = distance;
                }
            }
            // If no player has met the criteria so far the closest base should be selected no matter how far it is. 
            if (target == null)
            {
                currentDist = float.MaxValue;
            }
            int i = 0;
            foreach (Transform g in EnemyManager.basePos)
            {
                
                if (g != null)
                {
                    i++;
                    float distance = getDistance(g.transform.position,g.gameObject);
                    if (distance < currentDist || distance < goStraightToBaseDist)
                    {
                        targetIsPlayer = false;
                        target = g.gameObject;
                        currentDist = distance;
                    }
                }
            }
        }
        // Should actually never happen but we do not want anything to crash
        if (target == null)
        {
            target = this.gameObject;
        }
        agent.SetDestination(target.transform.position);
    }
    // Gives the length of the Path from the agent to the goal. This is not the direct way but the way a nav mesh agent would take
    private float getDistance(Vector3 goal,GameObject target)
    {
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        if (target.GetComponent<BaseHealth>() != null)
        {
            Vector3 direction = goal - this.transform.position;
            direction.y = 0;
            direction.Normalize();
            Vector3 goal2 = goal;
            goal = goal - (target.GetComponent<CapsuleCollider>().radius + 1) * direction;
            //Debug.DrawLine(goal, goal2, Color.red);
        }
        if (agent.CalculatePath(goal, path))
        {
            Vector3[] corners = path.corners;
            float length = 0;
            for (int i = 0; i < corners.Length - 1; i++)
            {
                length += Vector3.Distance(corners[i], corners[i + 1]);
            }
            return length;
        }
        else
        {
            Debug.Log("No path found");
            // if a target can not be reached the method return the highest possible number
            return float.MaxValue;
        }
    }
    
    
    
    /// ///////////////////////////////////////// Methodes take care of the shot
    
    // gets the next point on the ground 
    private Vector3 getNextGroundPoint(Vector3 target, Vector3 startpoint)
    {
        RaycastHit[] hits=getSortedRaycasthits(startpoint, target - startpoint);

        for(int i=0;i<hits.Length;i++)
        {
            // the Laser did not hit any ground before its range ended
            if (hits[i].distance >= ShootLaserRange)
            {
                return startpoint + Vector3.Normalize(target - startpoint) * ShootLaserRange;
            }

            if (hits[i].collider.tag.Equals("Untagged"))
            {
                return hits[i].point;
            }
        }
        return startpoint + Vector3.Normalize(target - startpoint) * ShootLaserRange;
    }

    private RaycastHit[] getSortedRaycasthits(Vector3 start, Vector3 direction)
    {
        RaycastHit[] hits = Physics.RaycastAll(start,direction);
        hits = sort(hits);
        return hits;
    }

    // needed to sort all raytracing hits
    private RaycastHit[] sort(RaycastHit[] ray)
    {
        RaycastHit[] solution = new RaycastHit[ray.Length];
        for (int i = 0; i < ray.Length; i++)
        {
            int x = getMin(ray);
            solution[i] = ray[x];
            ray[x].distance = float.MaxValue;
        }
        return solution;
    }
    private int getMin(RaycastHit[] ray)
    {
        int x = 0;
        float min = float.MaxValue;
        for (int i = 0; i < ray.Length; i++)
        {
            if (ray[i].distance < min)
            {
                min = ray[i].distance;
                x = i;
            }
        }
        return x;
    }


    /// ////////////////////////////////////////////////

    //assuming we can see the target
    //this function first rotates the laser along the axis and then along the x-Axis with rotation constraints
    private Vector3 TrackTarget()
    {
        if (target == null)
            state = AIState.Walk;
        //raycast target
        Vector3 targetDirection = target.transform.position+(target.transform.lossyScale*0.5f) - LaserStart.position;
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(LaserStart.position, targetDirection, out hit, ShootLaserRange, laserLayerMask))
        {
            
            Vector3 hitPoint = hit.point;
            /*
            Vector3 hitPointDir = hitPoint - LaserStart.position;//direction to hitpoint    // same astargetDircetion
            Vector3 currentLaserDir = LaserStart.TransformDirection(0, 0, 1); //direction the laser is pointing //?
            
            // --------------- Y-Rotation ------------------------
            //project hitdir and laser dir on x-z-Plane
            Vector3 hitPointProjectedXZ = Vector3.ProjectOnPlane(hitPointDir, Vector3.up);
            Vector3 currentLaserDirProjectedXZ = Vector3.ProjectOnPlane(currentLaserDir, Vector3.up);

            //calculate angle on y-Axis
            float yAngle = Vector3.SignedAngle(currentLaserDirProjectedXZ, hitPointProjectedXZ, Vector3.up);

            //rotate towards target
            LaserPivot.Rotate(0, yAngle, 0, Space.World);

            // --------------- X-Rotation ------------------------
            currentLaserDir = LaserStart.TransformDirection(0, 0, 1); //direction the laser is pointing after rotation

            //project hitdir and laser dir on x-z-Plane
            Vector3 hitPointProjectedYZ = Vector3.ProjectOnPlane(hitPointDir, LaserPivot.up);
            Vector3 currentLaserDirProjectedYZ = Vector3.ProjectOnPlane(currentLaserDir, LaserPivot.up);

            //calculate angle on y-Axis
            float xAngle = Vector3.SignedAngle(currentLaserDirProjectedYZ, hitPointProjectedYZ, LaserPivot.up);

            //rotate towards target
            LaserPivot.Rotate(0,xAngle, 0, Space.Self);*/

            RotateTowards(target.transform);

            return hitPoint;
        }
        else
        {
            return Vector3.zero;
        }
    }

    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * aimTurnSpeed);
    }

    private void ShootLaser()
    { 
        // sets time for cooldown
        lastLaser = Time.time;
    }

    [ClientRpc]
    private void RpcSetLaser(Vector3 position, bool enabled, Color color)
    {
        laserRenderer.enabled = enabled;

        if (position == Vector3.zero)
            position = LaserStart.position;

        laserRenderer.SetPosition(1, LaserStart.InverseTransformPoint(position));
        laserRenderer.material.color = color;

        laserRenderer.material.SetColor("_EmissionColor", color * (enabled? 0.8f : 0.1f));
    }

    // Gives the length of the Path from the agent to the goal. This is not the direct way but the way a nav mesh agent would take
    private float getNavmeshDistance(Vector3 goal)
    {
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        if (agent.CalculatePath(goal, path))
        {
            Vector3[] corners = path.corners;
            float length = 0;
            for (int i = 0; i < corners.Length - 1; i++)
            {
                length += Vector3.Distance(corners[i], corners[i + 1]);
            }
            return length;
        }
        else
        {
            // if a target can not be reached the method return the highest possible number
            return float.MaxValue;
        }

    }

    private bool canSeeTarget()
    {
        //raycast target
        Vector3 targetDirection = target.transform.position - LaserStart.position;
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(LaserStart.position, targetDirection, out hit, ShootLaserRange, laserLayerMask))
        {
            return hit.collider.gameObject == target;
        }
        return false;
    }

    private bool targetInRange()
    {
        return Vector3.Distance(target.transform.position, transform.position) <= ShootLaserRange;
    }

    private bool laserOffCooldown()
    {
        return Time.time >= lastLaser + MaxCooldown;
    }

    /// ////////// Handle the damage and dieing of the Enemy

    public void buttonPressed(bool isFront,float cd)
    {
       // Debug.Log(GetComponent<Animator>().GetCurrentAnimatorClipInfo(1).Length);
        if (isFront)
        {
            animator.SetBool("FrontShot", true);
            buttonsPressed[0] = true;
        }
        else
        {
            animator.SetBool("BackShot", true);
            buttonsPressed[1] = true;
        }
    }
    public void buttonNotPressed(bool isFront)
    {
        if (isFront)
        {
            animator.SetBool("FrontShot", false);
            buttonsPressed[0] = false;
        }
        else
        {
            animator.SetBool("BackShot", false);
            buttonsPressed[1] = false;
        }
    }



    private void dieing()
    {
        agent.enabled = false;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

        this.gameObject.layer=0;
        animator.SetBool("Dead",true);
        LaserStart.GetComponent<LineRenderer>().enabled = false;
        state = AIState.Dead;
        // Destroy(gameObject);
        RpcDeathAnimation();
        Debug.Log(gameObject.name + " dead");
        
        //GetComponentInChildren<SkinnedMeshRenderer>().material.color = new Color(1,1,1,1);
    }
    [ClientRpc]
    public void RpcDeathAnimation()
    {
        StartCoroutine("coDieing");
    }
    IEnumerator coDieing()
    {
        for (int i = 0; i < animator.GetCurrentAnimatorClipInfo(0).Length*50; i++)
        {
            yield return new WaitForSeconds(0.02f);
        }
        for (int i = 0; i < 20; i++)
        {
            this.transform.localScale *= 0.8f;
            yield return new WaitForSeconds(0.02f);
        }

        Instantiate(deathParticle, transform.position + new Vector3(0, 0, 0), transform.rotation);
        Destroy(gameObject, 0.1f);
        Debug.Log(gameObject.name + " dead");
    }
}
