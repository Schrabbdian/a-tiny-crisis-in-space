﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class BasicEnemySpawner : NetworkBehaviour {
    public GameObject enemyPrefab;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    public override void OnStartServer()
    {

        GameObject enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
        NetworkServer.Spawn(enemy);
    }
}
