﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillParticleSystem : MonoBehaviour 
{
    [SerializeField]
    private ParticleSystem partSys;
	
    //Kill the GameObject if specified particle system has stopped
	void Update () 
    {
        if (!partSys.IsAlive())
        {
            Destroy(gameObject);
        }
		
	}
}
