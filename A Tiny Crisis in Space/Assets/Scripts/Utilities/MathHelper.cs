﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathHelper 
{
    private static int newtonMaxIterations = 200;

    //Solve Quartic function numerically using newton's method
    public static float SolveQuartic(float c0, float c1, float c2, float c3, float c4, float startValue)
    {
        // normal form: x^4 + A * x^3 + B * x^2 + C * x + D = 0
        float A = c1 / c0;
        float B = c2 / c0;
        float C = c3 / c0;
        float D = c4 / c0;

        //Derivative: 4 * x^3 + 3A * x^2 + 2B * x + C

        float t = startValue;
        float f_t = Mathf.Pow(t, 4) + A * Mathf.Pow(t, 3) + B * Mathf.Pow(t, 2) + C * t + D;
        float fd_t = 4 * Mathf.Pow(t, 3) + 3 * A * Mathf.Pow(t, 2) + 2 * B * t + C;

        for (int i = 0; i < newtonMaxIterations && Mathf.Abs(f_t) > 0.05f; i++)
        {
            //Newton iteration: t = t - f(t)/f'(t)
            t = t - f_t / fd_t;

            f_t = Mathf.Pow(t, 4) + A * Mathf.Pow(t, 3) + B * Mathf.Pow(t, 2) + C * t + D;
            fd_t = 4 * Mathf.Pow(t, 3) + 3 * A * Mathf.Pow(t, 2) + 2 * B * t + C;
        }

        return t;
    }
}
