﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEffect : MonoBehaviour 
{
    // The Corotine started last
    private IEnumerator lastBlink;

    private UnityEngine.UI.Image img;

    [SerializeField]
    private float blinkFrames = 30;

    void Start()
    {
        img = GetComponent<UnityEngine.UI.Image>();
    }

    public void StartBlink(Color targetColor)
    {
        // Blinks if hit
        if (lastBlink != null)
        {
            StopCoroutine(lastBlink);
        }
        lastBlink = Blink(targetColor);
        StartCoroutine(lastBlink);
    }

    IEnumerator Blink(Color targetColor)
    {
        Color baseColor = targetColor; baseColor.a = 0.0f;

        Color change = (baseColor - targetColor) / blinkFrames * 2;
        for (int i = 0; i < blinkFrames; i++)
        {
            if (i < blinkFrames / 2)
            {
                img.color -= change;
            }
            else
            {
                img.color += change;
            }
            yield return null;
        }
        img.color = baseColor;
    }
}
