﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Cameras;

[RequireComponent(typeof(MergeHealth))]
public class UpperMerge : NetworkBehaviour
{
    public Vector3 cameraOffset;
    public Vector3 positionOffset;
    public Transform target;

    private Camera mainCamera;

    [SyncVar] private Vector3 lookDirection;

    public override void OnStartLocalPlayer()
    {
        mainCamera = Camera.main;
        //set camera to follow this object locally
        FreeLookCam freeLookCam = mainCamera.transform.parent.parent.GetComponent<FreeLookCam>();
        freeLookCam.SetTarget(transform);
        freeLookCam.m_UpdateType = AbstractTargetFollower.UpdateType.LateUpdate;
        
        
        //change camera pivot settings
        mainCamera.transform.parent.localPosition = cameraOffset;
    }
	
	// Update is called once per frame
	void Update () {
        if(!target)
            return;

        //update positions client-side
        transform.position = target.TransformPoint(positionOffset);

        //correct rotation
        transform.rotation = Quaternion.AngleAxis(90, target.forward) * target.rotation;

	    float yAngle;

	    if (isLocalPlayer)//if local player, get yAngle from camera direction and send to server
	    {
            //adjust look rotation
            Vector3 camForward = Vector3.ProjectOnPlane(mainCamera.transform.forward, transform.up);
            Vector3 playerForward = Vector3.ProjectOnPlane(transform.forward, transform.up);
            yAngle = -Vector3.SignedAngle(camForward, playerForward, transform.up);
            CmdSetLookDirection(mainCamera.transform.forward);
	    }
        else//if not local player, get other players look direction from syncvar
	    {
	        Vector3 camForward = Vector3.ProjectOnPlane(lookDirection, transform.up);
	        Vector3 playerForward = Vector3.ProjectOnPlane(transform.forward, transform.up);
	        yAngle = -Vector3.SignedAngle(camForward, playerForward, transform.up);
        }

        //apply user rotation
        Quaternion yRotation = Quaternion.AngleAxis(yAngle, transform.up);
        transform.rotation = yRotation * transform.rotation;

    }

    [ClientRpc]
    public void RpcAssignTarget(NetworkInstanceId id)
    {
        //find local gameobject
        GameObject targetObject = ClientScene.FindLocalObject(id);

        //find attach point
        MergeController mc = targetObject.GetComponent<MergeController>();
        target = mc.AttachPoint;

        //copy health of lower component
        Health otherHealth = targetObject.GetComponent<Health>();
        GetComponent<MergeHealth>().CopyOf = otherHealth;
    }

    [Command]
    private void CmdSetLookDirection(Vector3 lookDirection)
    {
        this.lookDirection = lookDirection;
    }
}
