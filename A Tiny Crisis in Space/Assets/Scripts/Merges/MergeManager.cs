﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

public class MergeManager : NetworkBehaviour
{

    public GameObject MergeObjectTop;

    public GameObject MergeObjectBottom;

    public GameObject NormalPlayerObject;

    public Vector3 DemergeJump = new Vector3(0, 30, 10);

    // Update is called once per frame
    void Update()
    {
        List<PlayerIngameInfo> infos = CustomLobbyManager.s_Singleton.GetServerSidePlayerInfos();
        //check each player pair
        foreach (PlayerIngameInfo player1 in infos)
        {
            foreach (PlayerIngameInfo player2 in infos)
            {
                MergeController mc1 = player1.MergeController;
                MergeController mc2 = player2.MergeController;

                if (mc1 == null || mc2 == null)//null check. can happen, when a player hasnt connected yet
                    continue;

                //dont merge same player
                if (mc1 == mc2)
                {
                    continue;
                }


                //if they are merged already, demerge them
                if (mc1.Merged && mc2.Merged)
                {
                    //check that mc1 and mc2 are merged with each other
                    if (mc1.partner.Equals(mc2))
                    {
                        if (player1.Health.IsDead() || player2.Health.IsDead()
                            || mc1.WantMerge || mc2.WantMerge)
                        {
                            ReplaceWithNormal(player1);
                            ReplaceWithNormal(player2);
                        }
                    }
                }
                //otherwise, check the distances of the 2 players
                else if (!mc1.Merged && !mc2.Merged)
                {
                    //check if the pair wants to merge
                    if (mc1.WantMerge && mc2.WantMerge)
                    {
                        float distance = Vector3.Distance(mc1.transform.position, mc2.transform.position);
                        if (distance <= mc1.MaxMergeDistance && distance <= mc2.MaxMergeDistance)
                        {
                            //lower player is always the person that has pushed the button first
                            float player1TimePressed = mc1.GetMergeRequestTime();
                            float player2TimePressed = mc2.GetMergeRequestTime();

                            GameObject topObj;
                            GameObject botObj;

                            float mergedHealth = player1.Health.GetCurrentHealth() + player2.Health.GetCurrentHealth();

                            if (player1TimePressed <= player2TimePressed)
                            {
                                topObj = ReplaceWithMerge(player2, MergeObjectTop, mergedHealth);
                                botObj = ReplaceWithMerge(player1, MergeObjectBottom, mergedHealth);
                            }
                            else
                            {
                                topObj = ReplaceWithMerge(player1, MergeObjectTop, mergedHealth);
                                botObj = ReplaceWithMerge(player2, MergeObjectBottom, mergedHealth);
                            }

                            //assign the object target to attach to (specific to this merge)
                            NetworkInstanceId botID = botObj.GetComponent<NetworkIdentity>().netId;
                            UpperMerge upperMerge = topObj.GetComponent<UpperMerge>();
                            upperMerge.RpcAssignTarget(botID);

                            MergeController botMc = botObj.GetComponent<MergeController>();
                            MergeController topMc = topObj.GetComponent<MergeController>();
                            topMc.partner = botMc;
                            botMc.partner = topMc;

                            Debug.Log("Merged players: " + mc1.gameObject.name + ", " + mc2.gameObject.name);
                        }
                    }
                }
            }
        }
    }

    //replace the player with the merged part
    private GameObject ReplaceWithMerge(PlayerIngameInfo player, GameObject mergePrefab, float mergedHealth)
    {
        GameObject playerObj = player.PlayerObject;

        GameObject mergeObj = Instantiate(mergePrefab, playerObj.transform.position, Quaternion.identity);

        Destroy(playerObj);

        //transfer both players normal health to merged health
        //make sure we dont kill the merge when they are merging
        if (mergedHealth < 1)
            mergedHealth = 1;

        mergeObj.GetComponent<Health>().SetStartHealth(mergedHealth);

        //rename and set id
        PlayerProperties pOld = playerObj.GetComponent<PlayerProperties>();
        PlayerProperties pNew = mergeObj.GetComponent<PlayerProperties>();
        pNew.ID = pOld.ID;
        pNew.playerName = pOld.playerName;

        CustomLobbyManager.s_Singleton.ReplacePlayer(player, mergeObj);

        return mergeObj;
    }

    //replace the player with the normal player model
    private void ReplaceWithNormal(PlayerIngameInfo player)
    {
        GameObject playerObj = player.PlayerObject;

        GameObject normalPlayerObj = Instantiate(NormalPlayerObject, playerObj.transform.position, playerObj.transform.rotation);

        bool isUpperPlayer = playerObj.GetComponent<UpperMerge>() != null;

        Destroy(playerObj);

        //transfer merge health to normal player health
        float mergedHealth = playerObj.GetComponent<Health>().GetCurrentHealth();
        //if the merged object has 0 health both players spawn with 0 health
        normalPlayerObj.GetComponent<Health>().SetStartHealth(Mathf.Ceil(mergedHealth / 2f));

        //rename and set id
        PlayerProperties pOld = playerObj.GetComponent<PlayerProperties>();
        PlayerProperties pNew = normalPlayerObj.GetComponent<PlayerProperties>();
        pNew.ID = pOld.ID;
        pNew.playerName = pOld.playerName;

        CustomLobbyManager.s_Singleton.ReplacePlayer(player, normalPlayerObj);

        if (isUpperPlayer)
        {
            if (!playerObj.GetComponent<Health>().IsDead())
            {
                //if its a voluntarily demerge, he jumps
                Vector3 demergeJump = playerObj.transform.forward * DemergeJump.z + playerObj.transform.up * DemergeJump.y;

                normalPlayerObj.GetComponent<PlayerMovementController>().RpcSetKnockback(demergeJump);
            }
        }
    }
}
