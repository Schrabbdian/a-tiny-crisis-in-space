﻿using UnityEngine;
using UnityEngine.Networking;

public class MergeController : NetworkBehaviour
{
    public float MaxMergeDistance = 5f;
    [SerializeField]
    private string attachBone;
    public bool Merged;
    [SerializeField]
    private string mergeButton;
    [SerializeField]
    private GameObject mergeIndicatorObject;

    public MergeController partner = null;

    private Health myHealth;

    [SyncVar(hook = "UpdateMergeRequestTime")]
    private bool wantMerge;

    private bool canMerge = true;//bool that waits until person has let go of the button

    private float mergeRequestTime;

    //server check who pressed the button first out of the two
    private void UpdateMergeRequestTime(bool value)
    {
        if (!isServer)
        {
            wantMerge = value;
            return;
        }

        
        //when wantmerge changes from false to true
        if (value)
        {
            mergeRequestTime = Time.time;
        }
    }

    public float GetMergeRequestTime()
    {
        return mergeRequestTime;
    }

	// Use this for initialization
	private void Start ()
	{
        if(mergeIndicatorObject)
	        mergeIndicatorObject.SetActive(false);

        //wantMerge = false;

        //we check if the person is holding down the button in the first frame
        //if yes he can only merge/demerge once he/she has let go
	    if (Input.GetButtonDown(mergeButton))
	    {
	        canMerge = false;
	    }

	    myHealth = GetComponent<Health>();
	}
	
	// Update is called once per frame
	private void Update ()
	{
        //resolve input if local player
	    if (isLocalPlayer)
	    {
            //if were dead just act as if we let go of the button
	        if (myHealth.IsDead() || Input.GetButtonUp(mergeButton))
	        {
	            CmdWantMerge(false);

                //we update wantMerge here clientside as fast as possible
	            wantMerge = false;
                
                //after the person has let go of the button he/she can merge again
	            canMerge = true;
	        }
	        else if (Input.GetButtonDown(mergeButton) && canMerge)
	        {
	            CmdWantMerge(true);
	            wantMerge = true;
	        }
        }

        //rotate indicator object to surface normal
	    if (wantMerge)
	    {
            Ray ray = new Ray(transform.position + Vector3.up, Vector3.down * 5f);
	        RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                Vector3 surfacePoint = hit.point;
                Vector3 surfaceNormal = hit.normal;

                //place just above floor
                mergeIndicatorObject.transform.position = surfacePoint + surfaceNormal * 0.1f;

                //Rotate so the forward vector faces the normal direction
                Quaternion rotateToNormal = Quaternion.FromToRotation(Vector3.forward, surfaceNormal);
                mergeIndicatorObject.transform.rotation = rotateToNormal;
            }
	    }

        //scale indicator according to max merge distance
	    if (!Merged)
	    {
	        mergeIndicatorObject.transform.localScale = Vector3.one * MaxMergeDistance * 2.2f; //about right
	    }
	    
        //show indicator for this player
        if (mergeIndicatorObject)
	        mergeIndicatorObject.SetActive(wantMerge);
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 1, 0, 0.5f);
        Gizmos.DrawSphere(transform.position, MaxMergeDistance);
    }

    [Command]
    private void CmdWantMerge(bool value)
    {
        wantMerge = value;
    }

    public bool WantMerge
    {
        get { return wantMerge; }
    }

    //finds the bone with the specified name, if no name is specified just returns root transform
    public Transform AttachPoint
    {
        get
        {
            Transform t = transform.Find(attachBone);
            if (!t)
            {
                return transform;
            }
            else
            {
                return t;
            }
        }
    }
}
