﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Cameras;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(PlayerMovementController))]
public class PlayerUserInput : NetworkBehaviour
{
    private PlayerMovementController playerMovementController; // A reference to the ThirdPersonCharacter on the object
    private Transform cam;                  // A reference to the main camera in the scenes transform
    private Vector3 camForward;             // The current forward direction of the camera

    private Vector3 move;
    private bool jumpInput;                      // the world-relative desired move direction, calculated from the camForward and user input.


    private void Start()
    {
        // get the transform of the main camera
        if (Camera.main != null)
        {
            cam = Camera.main.transform;
            
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        }

        // get the third person character ( this should never be null due to require component )
        playerMovementController = GetComponent<PlayerMovementController>();

        //make the camera follow this player
        if (isLocalPlayer)
        {
            FreeLookCam freeLookCam = cam.transform.parent.parent.GetComponent<FreeLookCam>();
            freeLookCam.SetTarget(transform);
            freeLookCam.m_UpdateType = AbstractTargetFollower.UpdateType.FixedUpdate;
        }
    }


    private void Update()
    {
        if (!isLocalPlayer)
            return;

        if (!jumpInput)
        {
            jumpInput = CrossPlatformInputManager.GetButtonDown("Jump");
        }
    }


    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        if (!isLocalPlayer)
            return;

        // read inputs
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");

        // calculate move direction to pass to character
        if (cam != null)
        {
            // calculate camera relative direction to move:
            camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
            move = v * camForward + h * cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            move = v * Vector3.forward + h * Vector3.right;
        }

        // pass all parameters to the character control script
        playerMovementController.Move(move, jumpInput);
        jumpInput = false;
    }

}
