﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;

public class ThrowBombs : NetworkBehaviour
{
    [Header("Cooldown")] public float Cooldown;

    [Header("Thrown Object")]
    public GameObject bombPrefab;

    [Header("Throw Properties")]
    public Transform throwOriginLeft;
    public Transform throwOriginRight;
    public float throwSpeed = 5.0f;
    public float verticalAngle = 45.0f;
    public float horizontalAngle = 0.0f;
    public float angleModifier = 30f;

    [Header("Component References")]
    public DrawTrajectory trajectoryVisualizer;
    public Animator animator;
    public FastNetworkAnimator networkAnimator;

    
    //Internal Component References
    private Camera mainCam;

    //Internal Throwing Variables
    private Vector3 throwDir;
    private Vector3 throwPoint;
    private bool throwLeft = true;
    private float nextThrow = -1000;

    void Start()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (Camera.main != null)
        {
            mainCam = Camera.main;
        }
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }


        throwPoint = throwLeft ? throwOriginLeft.position : throwOriginRight.position;

        
        //change speed depending on camera direction
        float throwAngleModifier = mainCam.transform.eulerAngles.x;
        if (throwAngleModifier >= 180)
        {
            throwAngleModifier = -360 + throwAngleModifier;
        }
        throwAngleModifier = Mathf.Clamp(throwAngleModifier / verticalAngle, 0f, 1f);
        throwAngleModifier *= angleModifier;

        throwDir = Quaternion.AngleAxis(verticalAngle - throwAngleModifier, -mainCam.transform.right)
                   * Quaternion.AngleAxis(horizontalAngle, mainCam.transform.up) * mainCam.transform.forward;


        if (CrossPlatformInputManager.GetButton("Fire1"))
        {
            trajectoryVisualizer.startPos = throwPoint;
            trajectoryVisualizer.startVelocity = throwDir * throwSpeed;

            trajectoryVisualizer.enabled = true;
        }
        else
        {
            trajectoryVisualizer.enabled = false;
        }

        if (CrossPlatformInputManager.GetButtonUp("Fire1") && !OnCooldown())
        {
            CmdThrowBomb(throwPoint, throwDir * throwSpeed);

            //Set Animator Variables
            if (throwLeft) { networkAnimator.SetTrigger("ThrowLeft"); networkAnimator.animator.ResetTrigger("ThrowLeft"); }
            else { networkAnimator.SetTrigger("ThrowRight"); networkAnimator.animator.ResetTrigger("ThrowRight"); }

            throwLeft = !throwLeft;

            nextThrow = Time.time + Cooldown;
        }

    }

    private bool OnCooldown()
    {
        return Time.time < nextThrow;
    }

    [Command]
    void CmdThrowBomb(Vector3 position, Vector3 velocity)
    {
        GameObject bomb = Instantiate(bombPrefab, position, Quaternion.identity);
        bomb.GetComponent<Rigidbody>().velocity = velocity;

        NetworkServer.Spawn(bomb);
    }
}
