﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Cameras;
using UnityStandardAssets.CrossPlatformInput;

public class Shooting : NetworkBehaviour 
{
    public GameObject projectilePrefab;
    public Transform shootOrigin;
    public float projectileSpeed = 20.0f;
    public float fireCooldown = 0.1f;

    public LayerMask layerMask;

    Camera mainCam;
    [SerializeField]
    private Transform camPivot;
    [SerializeField]
    private PlayerHealth playerHealth;

    private float shootCooldown = 0.0f;
    private Vector3 shootDir;
    private float shootFlagCooldown = 0.0f;
    private bool networkStarted = false;

	public override void OnStartLocalPlayer() 
    {
        if (Camera.main != null)
        {
            mainCam = Camera.main;
            camPivot = mainCam.transform.parent;

            FreeLookCam flCam = camPivot.transform.parent.GetComponent<FreeLookCam>();

            networkStarted = true;
        }

        playerHealth = GetComponent<PlayerHealth>();
    }


	void Update () 
    {
        if (!isLocalPlayer)
            return;

        if (!networkStarted)
        {
            return;
        }

        //Decrement cooldowns
        shootCooldown = Mathf.Clamp01(shootCooldown - Time.deltaTime);
        shootFlagCooldown = Mathf.Clamp01(shootFlagCooldown - Time.deltaTime);

        //Shoot Button Pressed
        if (CrossPlatformInputManager.GetButton("Fire1") || CrossPlatformInputManager.GetAxis("Fire1") > 0.0f)
        {
            RaycastHit hit;

            //Determine Shooting Direction
            if (Physics.Raycast(camPivot.position, mainCam.transform.forward, out hit, 10000.0f, layerMask.value, QueryTriggerInteraction.Ignore))
            {
                shootDir = (hit.point - shootOrigin.position).normalized;
            }
            else
            {
                //If nothing was hit, pretend we hit something very far away
                Vector3 point = camPivot.position + mainCam.transform.forward * 10000.0f;
                shootDir = (point - shootOrigin.position).normalized;
            }

            //If No Cooldown & not in revive state, Fire Projectile
            if(CanShoot())
            {
                CmdShoot(shootDir, projectileSpeed);

                shootCooldown = fireCooldown;
            }

            shootFlagCooldown = 0.1f;//refresh shoot flag cooldown
        }
		
	}

    [Command]
    void CmdShoot(Vector3 direction, float speed)
    {
        GameObject projectile = Instantiate(projectilePrefab, shootOrigin.position, Quaternion.identity);
        projectile.GetComponent<Rigidbody>().velocity = direction * speed;

        NetworkServer.Spawn(projectile);
    }

    private bool CanShoot()
    {
        return shootCooldown <= 0.0f && !playerHealth.IsDead();
    }

    public bool IsShooting()
    {
        return playerHealth != null && !playerHealth.IsDead() && shootFlagCooldown > 0.0f;
    }

    public Vector3 GetShootDir()
    {
        return shootDir;
    }
}
