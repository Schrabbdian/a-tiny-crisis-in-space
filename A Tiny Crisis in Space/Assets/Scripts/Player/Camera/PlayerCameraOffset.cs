﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class PlayerCameraOffset : CameraOffset
{

    public CameraOffset.OffsetConfig ShoulderConfig;
    public CameraOffset.OffsetConfig RunConfig;

    private Shooting shooting;
	// Use this for initialization
	void Start ()
	{
	    shooting = GetComponent<Shooting>();
	}

    public override CameraOffset.OffsetConfig GetConfig()
    {
        return shooting.IsShooting() ? ShoulderConfig : RunConfig;
    }
}
