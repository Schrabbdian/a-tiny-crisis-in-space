﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class ConstantCameraOffset : CameraOffset
{

    public CameraOffset.OffsetConfig Config;

    public override CameraOffset.OffsetConfig GetConfig()
    {
        return Config;
    }
}
