﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CameraOffset : MonoBehaviour
    {
        [System.Serializable]
        public struct OffsetConfig
        {
            public float CameraZDistance;
            public Vector3 PivotOffset;
        }

        public abstract OffsetConfig GetConfig();
    }

