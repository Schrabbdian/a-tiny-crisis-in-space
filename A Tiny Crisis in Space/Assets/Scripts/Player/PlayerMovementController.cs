﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class PlayerMovementController : NetworkBehaviour
{
    [Header("Ground Movement")]
    [SerializeField]
    float turnSpeed;
    [SerializeField] float turnSpeedShooting;

    [Header("Jumping")]
    [SerializeField]
    float jumpStrength;
    [SerializeField] float gravityMultiplier;
    [SerializeField] float airControl;
    [SerializeField] float maxAirSpeed;
    [SerializeField] float turnSpeedAir;

    [Header("Ground Collision")]
    [SerializeField]
    float groundCheckDistance = 0.1f;
    [SerializeField] LayerMask groundMask;
    private Vector3 lastSafePosition;

    private Vector3 knockback;

    //Component References
    Rigidbody rigidBody;
    Animator animator;
    CapsuleCollider capsuleColl;
    Shooting shoot;
    PlayerHealth playerHealth; //todo integrate different movement constraints when downed
    MergeController mergeController;


    //Movement
    float turnAmount;
    float forwardAmount;

    //Collision / Ground
    Vector3 groundNormal;
    bool isGrounded;
    float original_groundCheckDistance;

    //Shooting
    bool isShooting;

    //Revive
    private bool isDown;

    void Start()
    {
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        capsuleColl = GetComponent<CapsuleCollider>();
        shoot = GetComponent<Shooting>();
        playerHealth = GetComponent<PlayerHealth>();
        mergeController = GetComponent<MergeController>();


        rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        original_groundCheckDistance = groundCheckDistance;

        lastSafePosition = transform.position;
    }

    public void Move(Vector3 move, bool jump)
    {

        // convert the world relative moveInput vector into a local-relative
        // turn amount and forward amount required to head in the desired
        // direction.
        if (move.magnitude > 1f) move.Normalize();
        Vector3 moveWorld = move;
        move = transform.InverseTransformDirection(move);

        CheckGroundStatus();
        CheckIsDown();

        move = Vector3.ProjectOnPlane(move, groundNormal);
        turnAmount = Mathf.Atan2(move.x, move.z);
        forwardAmount = move.z;

        // control and velocity handling is different when grounded and airborne:
        if (isGrounded)
        {
            //cant jump when downed
            if(!isDown)
                HandleGroundedMovement(jump);
        }
        else
        {
            HandleAirborneMovement(moveWorld);
        }

        if (shoot)
            isShooting = shoot.IsShooting();

        //When shooting, the player cant turn so we use the turnAmount float to represent strafing left/right
        if (isShooting)
        {
            turnAmount = move.x;
        }
        
        Turn();


        // send input and other state parameters to the animator
        UpdateAnimator(move);

        ConsumeKnockback();
    }

    protected virtual void UpdateAnimator(Vector3 move)//todo subclass for merged player controller
    {
        // update the animator parameters
        animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        animator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);
        animator.SetBool("OnGround", isGrounded);
        animator.SetBool("IsDown", isDown);

        if (shoot)
        {
            animator.SetBool("IsShooting", isShooting);

            if (isShooting)
            {
                //calculate the vertical shooting angle
                Vector3 shootDir = shoot.GetShootDir();

                animator.SetFloat("ShootAngle", Mathf.Lerp(animator.GetFloat("ShootAngle"), shootDir.y, Time.deltaTime * 15.0f));
            }
            else
            {
                //return smoothly to zero
                animator.SetFloat("ShootAngle", Mathf.Lerp(animator.GetFloat("ShootAngle"), 0.0f, Time.deltaTime * 2.0f));
            }
        }
        
        if (!isGrounded)
        {
            animator.SetFloat("Jump", rigidBody.velocity.y);
        }

        animator.SetBool("RequestMerge", mergeController.WantMerge);
    }

    public void OnAnimatorMove()
    {
        if (isGrounded && Time.deltaTime > 0)
        {
            Vector3 v = animator.deltaPosition / Time.deltaTime;
            // we preserve the existing y part of the current velocity.
            v.y = rigidBody.velocity.y;

            rigidBody.velocity = v;
        }
    }

    void HandleGroundedMovement(bool jump)
    {
        // check whether conditions are right to allow a jump:
        if (jump && (animator.GetCurrentAnimatorStateInfo(0).IsName("GroundMovement") || animator.GetCurrentAnimatorStateInfo(0).IsName("Strafing")))
        {
            // jump up, super star!
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, jumpStrength, rigidBody.velocity.z);
            isGrounded = false;
            animator.applyRootMotion = false;
            groundCheckDistance = 0.01f;
        }
    }

    void HandleAirborneMovement(Vector3 move)
    {
        // apply extra gravity from multiplier:
        Vector3 extraGravityForce = (Physics.gravity * gravityMultiplier) - Physics.gravity;
        rigidBody.AddForce(extraGravityForce * rigidBody.mass);

        groundCheckDistance = rigidBody.velocity.y < 0 ? original_groundCheckDistance : 0.01f;

        //Enable control over velocity while in air
        Vector3 velDelta = move * airControl * Time.deltaTime;
        Vector3 velAir = rigidBody.velocity + velDelta;
        velAir.y = 0.0f;
        velAir = Vector3.ClampMagnitude(velAir, maxAirSpeed);

        rigidBody.velocity = new Vector3(velAir.x, rigidBody.velocity.y, velAir.z);
    }

    void Turn()
    {
        //Turning
        float speed = turnSpeed;
        if (!isGrounded) { speed = turnSpeedAir; }

        if (!isShooting)
        {
            //Turn into walking direction
            rigidBody.MoveRotation(rigidBody.rotation * Quaternion.Euler(Vector3.up * turnAmount * speed * Time.deltaTime));
        }
        else
        {
            //Face shooting direction
            Vector3 shootDir = shoot.GetShootDir();

            Vector3 targetDir = Vector3.ProjectOnPlane(shootDir, transform.up);

            rigidBody.MoveRotation(Quaternion.Slerp(rigidBody.rotation, Quaternion.LookRotation(targetDir, Vector3.up), turnSpeedShooting * Time.deltaTime));
        }
    }

    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundCheckDistance));
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
        {
            groundNormal = hitInfo.normal;
            isGrounded = true;
            animator.applyRootMotion = true;

            //save last safe spot and move him a little bit back, so the player doesnt instantly drop down again
            lastSafePosition = transform.position - transform.forward + Vector3.up * 0.5f;
        }
        else
        {
            isGrounded = false;
            groundNormal = Vector3.up;
            animator.applyRootMotion = false;
        }
    }

    public void CheckIsDown()
    {
        isDown = playerHealth && playerHealth.IsDead();
    }

    public void ResetToLastSafePosition()
    {
        transform.position = lastSafePosition;
        rigidBody.velocity = Vector3.zero;
    }

    [ClientRpc]
    public void RpcSetKnockback(Vector3 velocity)
    {
        knockback = velocity;
    }

    private void ConsumeKnockback()
    {
        if (knockback != Vector3.zero)
        {
            rigidBody.velocity = knockback;
            knockback = Vector3.zero;
        }
    }
}
