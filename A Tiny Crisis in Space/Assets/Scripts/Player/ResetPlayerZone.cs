﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ResetPlayerZone : NetworkBehaviour {

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            NetworkIdentity nID = coll.gameObject.GetComponent<NetworkIdentity>();

            if (nID && nID.localPlayerAuthority)
            {
                StartCoroutine(ResetPlayer(coll));
            }
        }
    }

    private IEnumerator ResetPlayer(Collider coll)
    {
        PlayerMovementController pc = coll.gameObject.GetComponent<PlayerMovementController>();
        pc.ResetToLastSafePosition();

        yield return new WaitForSecondsRealtime(0.1f);

        //set health to 0
        coll.gameObject.GetComponent<Health>().TakeDamage(1000, Health.DamageType.World);
        
    }
}
