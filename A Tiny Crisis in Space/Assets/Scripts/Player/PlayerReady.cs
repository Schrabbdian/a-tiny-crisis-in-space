﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerReady : NetworkBehaviour
{
    public static PlayerReady localInstance;
    private static int amountReady = 0;

    public static void Reset()
    {
        amountReady = 0;
    }

    void Start()
    {
        if (isLocalPlayer)
        {
            localInstance = this;
        }
    }

    public static int AmountOfPlayersReady()
    {
        return amountReady;
    }

    void OnDestroy()
    {
        
    }


    [Command]
    public void CmdSetReady()
    {
        amountReady++;
    }
}
