﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class DrawTrajectory : MonoBehaviour
{
    public Vector3 startPos;
    public Vector3 startVelocity;
    public int steps = 20;
    public LayerMask layerMask;

    [Header("Marker at Impact Position")]
    public GameObject impactMarkerPrefab;

    private LineRenderer lineRenderer;
    private Transform impactMarker;

	void Start () 
    {
		lineRenderer = GetComponent<LineRenderer>();

        if (impactMarkerPrefab)
        {
            GameObject markerObj = Instantiate(impactMarkerPrefab);
            impactMarker = markerObj.transform;

            markerObj.hideFlags = HideFlags.HideAndDontSave;
        }
	}

	void Update () 
    {
        if (startVelocity == Vector3.zero || steps < 2) return;

        lineRenderer.positionCount = steps;

        Vector3 pos = startPos;
        Vector3 vel = startVelocity;

        Vector3 normal = Vector3.up;

        for (int i = 0; i < steps; i++)
        {
            lineRenderer.SetPosition(i, pos);

            Vector3 oldPos = pos;
            pos += vel * Time.fixedDeltaTime;
            vel += Physics.gravity * Time.fixedDeltaTime;

            //stop early if we hit a surface
            RaycastHit hitInfo;
            Vector3 newPos = pos;
            Vector3 toNew = newPos - oldPos;
            
            
            if (Physics.Raycast(oldPos, toNew.normalized, out hitInfo, toNew.magnitude, layerMask.value, QueryTriggerInteraction.Ignore))
            {
                lineRenderer.positionCount = i + 1;
                lineRenderer.SetPosition(i, hitInfo.point);
                normal = hitInfo.normal;

                break;
            }
        }

        //Place marker at final point of line renderer
        if (impactMarker)
        {
            Vector3 markerPos = lineRenderer.GetPosition(lineRenderer.positionCount - 1) + normal * 0.1f;
            impactMarker.transform.position = markerPos;

            //Rotate so the forward vector faces the normal direction
            Quaternion rotateToNormal = Quaternion.FromToRotation(Vector3.forward, normal);
            impactMarker.rotation = rotateToNormal;
        }
	}

    void OnEnable()
    {
        if (lineRenderer)
        {
            lineRenderer.enabled = true;
        }

        if (impactMarker)
        {
            impactMarker.gameObject.SetActive(true);
        }
    }

    void OnDisable()
    {
        if (lineRenderer)
        {
            lineRenderer.enabled = false;
        }

        if (impactMarker)
        {
            impactMarker.gameObject.SetActive(false);
        }
    }
}
