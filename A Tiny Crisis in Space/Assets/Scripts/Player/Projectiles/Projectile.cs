﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour 
{
    public float timeToLive = 5.0f;
    public float Damage = 10f;
    public GameObject explosionEffect;

    //this is to prevent a bullet from damaging 2 things in a single frame
    private bool hasHitSomething = false;

	void Update () 
    {
        timeToLive -= Time.deltaTime;

        if (timeToLive <= 0.0f) 
        {
            Destroy(gameObject);
        }
	}

    void OnCollisionEnter(Collision coll)
    {
        Instantiate(explosionEffect, transform.position, Quaternion.identity);

        if (!hasHitSomething)
        {
            Health health = coll.gameObject.GetComponent<Health>();
            if (health && !(health is BaseHealth))
            {
                health.TakeDamage(Damage, Health.DamageType.PlayerProjectile);
                hasHitSomething = true;
            }
        }
        
        //trick to always spawn a particle effect
        //problem: server destroys object at client before it reaches wall 
        //      -> no particle effect is spawned
        //solution: deactivate gameobject, server sends destroy message a bit later
        //          -> projectile always reaches wall client side and spawns the particle
        gameObject.SetActive(false);
        Destroy(gameObject, 0.1f);
    }
}
