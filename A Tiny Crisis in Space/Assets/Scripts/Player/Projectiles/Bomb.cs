﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Bomb : NetworkBehaviour 
{
    public float timeToLive = 5.0f;
    public GameObject explosionEffect;
    public float DamageRadius = 2;
    public float DamageForce;
    public int Damage = 25;
    public LayerMask layerMask;

    private bool notExploded = true;
    private bool active = true;

    void Update()
    {
        timeToLive -= Time.deltaTime;

        if (timeToLive <= 0.0f && notExploded)
        {
            notExploded = false;
            StartCoroutine(Explode());
        }
    }

    //deactivate bombs that are inside robuckets
    public void Deactivate()
    {
        active = false;
    }


    private IEnumerator Explode()
    {
        while (transform.localScale.magnitude < 2.5f)
        {
            transform.localScale += 2.5f * Vector3.one * Time.deltaTime;
            yield return null;
        }
        //stop coroutine and destroy object
        if (!active)
        {
            Destroy(gameObject);
            yield break;
        }
            
        Instantiate(explosionEffect, transform.position, Quaternion.identity);
        //do not deactivate whole object, otherwise coroutine is also stopped
        gameObject.SetActive(false);

        if (isServer)
        {
            Collider[] overlaps = Physics.OverlapSphere(transform.position, DamageRadius, layerMask);
            foreach (Collider collider in overlaps)
            {
                Rigidbody r = collider.gameObject.GetComponent<Rigidbody>();
                if (r)
                {
                    r.AddExplosionForce(DamageForce, transform.position, DamageRadius);
                }

                //if is enemy then damage
                if(collider.gameObject.CompareTag("Base"))
                    continue;

                Health h = collider.gameObject.GetComponent<Health>();
                if (h && !(h is PlayerHealth))
                {
                    h.TakeDamage(Damage, Health.DamageType.PlayerBomb);
                }
            }

            //see projectile.cs for explanation
            Destroy(gameObject, 0.1f);
        }
    }

}
