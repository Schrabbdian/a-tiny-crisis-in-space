﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Health), typeof(MergeController), typeof(NetworkIdentity))]
public class RegisterPlayer : MonoBehaviour {
	// Use this for initialization
	public void Start ()
	{
	    Health h = GetComponent<Health>();
	    MergeController m = GetComponent<MergeController>();
	    NetworkIdentity id = GetComponent<NetworkIdentity>();
	    NetworkConnection connection = id.isServer ? id.connectionToClient : id.clientAuthorityOwner;

        PlayerIngameInfo info = new PlayerIngameInfo(gameObject.name, connection, this.gameObject, h, m);

        CustomLobbyManager.s_Singleton.RegisterPlayerObject(info);
	}
	
}
