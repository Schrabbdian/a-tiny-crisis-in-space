﻿using UnityEngine.Networking;

public interface IPlayerAdded
{
    void OnServerAddPlayer(NetworkConnection connection, short playerControllerId);
}
