﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FastNetworkAnimator : NetworkAnimator
{
    //public float CustomSendInterval = 0.05f;

    public override float GetNetworkSendInterval()
    {
        return 0.05f;
    }
}
