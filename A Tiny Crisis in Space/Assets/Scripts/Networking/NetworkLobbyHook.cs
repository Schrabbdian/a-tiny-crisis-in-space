﻿using UnityEngine;
using Prototype.NetworkLobby;
using System.Collections;
using UnityEngine.Networking;

public class NetworkLobbyHook : LobbyHook
{
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();
        PlayerProperties playerProperties = gamePlayer.GetComponent<PlayerProperties>();

        playerProperties.playerName = lobby.playerName;
        playerProperties.ID = LobbyPlayerList._instance.FindPlayerID(lobby.playerName);
    }
}
