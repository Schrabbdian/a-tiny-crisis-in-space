﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkRotation : NetworkBehaviour
{
    [SyncVar] private Quaternion rotationQuaternion;
    public float LerpSpeed;
	
	// Update is called once per frame
	void Update () {
        //read from local player and send to server
	    if (isLocalPlayer)
	    {
	        Quaternion rotation = transform.rotation;
            CmdSetRotation(rotation);
	    }
	    else
	    {
	        //lerp to new rotation
	        transform.rotation = Quaternion.Slerp(transform.rotation, rotationQuaternion, LerpSpeed * Time.deltaTime);//todo slerp
	    }
	}

    [Command]
    private void CmdSetRotation(Quaternion rotation)
    {
        rotationQuaternion = rotation;
    }
}
