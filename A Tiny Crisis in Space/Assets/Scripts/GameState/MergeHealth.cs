﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class MergeHealth : Health
{
    public Health CopyOf;

    [Header("Damage Visual Effect")]
    [SerializeField]
    private Color blinkColor;
    private DamageEffect dmgEffect;

    protected override void OnZeroHealth()
    {
       //dont do anything

    }

    public override float GetCurrentHealth()
    {
        if (CopyOf)
        {
            return CopyOf.GetCurrentHealth();
        }

        return base.GetCurrentHealth();
    }

    void Update()
    {
        if (CopyOf)
        {
            float changedHealth = GetCurrentHealth();
            OnHealthChanged(changedHealth);

            currentHealth = changedHealth;
        }
    }

    protected override void OnHealthChanged(float changedHealth)
    {
        base.OnHealthChanged(changedHealth);

        if (!isLocalPlayer) return;

        //if health is being reduced
        if (!IsDead() && (currentHealth - changedHealth) > 1.0f)
        {
            //lazy initialize component references
            if (dmgEffect == null)
            { dmgEffect = GameObject.FindObjectOfType<DamageEffect>(); }


            //if references set up
            if (dmgEffect != null)
            {
                //activate damage overlay blinking
                dmgEffect.StartBlink(blinkColor);
            }
        }
    }
}
