﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

public class GameStateManager : NetworkBehaviour, IResettable
{
    private int PlayersNeededToStart = 1;
    [SerializeField] private int PreWaveCountdown = 10;

    private GameState state = GameState.PreGame;
    public enum GameState
    {
        PreGame, PreWave, Wave, Won, Lost
    }
    
    //server-side only
    private int currentWaveIndex = 0;
    private List<Health> baseHealths = new List<Health>();
    private CustomLobbyManager networkManager;
    [SerializeField]
    private EnemyManager enemyManager;

    [SyncVar]
    private int PlayersReady = 0;//only server needs to know that enough players are ready to start the game

    //client-side
    private float timer;

    [Header("Debug")]
    public bool showDebugGUI = false;

    public override void OnStartClient()
    {
        //CmdRequestCurrentState();
    }

    public override void OnStartServer()
    {
        networkManager = (CustomLobbyManager) NetworkManager.singleton;
        PlayersNeededToStart = networkManager._playerNumber;

        //get health components of bases
        Transform[] bases = EnemyManager.basePos;
        foreach (Transform playerBase in bases)
        {
            Health h = playerBase.GetComponent<Health>();
            if (h)
            {
                baseHealths.Add(h);
            }
            else
            {
                Debug.LogError(playerBase.name + " is missing a Health Component!");
            }
            
        }

        //reset ready player list
        PlayerReady.Reset();
    }
    
    public void OnDestroy()
    {
        if(isServer)
        networkManager.ResetPlayerInfos();
        PlayersReady = 0;

    }

    // Update is called once per frame
    void Update()
    {

        //countdown timer only in certain states
        switch (state)
        {
            case GameState.PreWave:
            case GameState.Wave:
                timer -= Time.deltaTime;
                break;
        }

        //do state transition only on server and call rpcs to update clients
        if (!isServer)
        {
            return;
        }

        switch (state)
        {
            //wait until all players join
            case GameState.PreGame:
                if (AllPlayersReady())
                {
                    RpcSendState(GameState.PreWave, PreWaveCountdown);
                }
                break;

            //all players have joined and countdown happens
            case GameState.PreWave:
                //once the countdown is 0 spawn the wave server side
                //rpc notifies clients for timers and other behaviours
                if (timer <= 0)
                {
                    RpcSendState(GameState.Wave, enemyManager.GetWave(currentWaveIndex).WaveTime);
                    enemyManager.spawnWave(currentWaveIndex);
                }
                break;

            //wave spawns and timer counts down
            case GameState.Wave:
                //check objectives (one objective destroyed is enough to lose)
                foreach (Health baseHealth in baseHealths)
                {
                    if (baseHealth.IsDead())
                    {
                        RpcSendState(GameState.Lost, timer);
                    }
                }

                //check player health (all players must be down to lose)
                List<PlayerIngameInfo> players = networkManager.GetServerSidePlayerInfos();
                bool allDown = true;
                foreach (PlayerIngameInfo player in players)
                {
                    if (player.Health && !player.Health.IsDead())
                    {
                        allDown = false; //if one player is still alive
                    }
                }

                if (allDown)
                {
                    RpcSendState(GameState.Lost, timer);
                }

                //wave is over
                if (timer <= 0)
                {
                    //check if any waves left
                    if (currentWaveIndex + 1 < enemyManager.NumberOfWaves)
                    {
                        //set next wave to spawn
                        currentWaveIndex++;
                        RpcSendState(GameState.PreWave, PreWaveCountdown);
                    }
                    else
                    {
                        //wait for all enemies to be killed
                        if (AllEnemiesDead())
                            RpcSendState(GameState.Won, timer);
                    }
                }
                break;

            case GameState.Won:
                //todo show win messgage and/or end game stats
                //Debug.Log("Won! woooo");
                break;

            case GameState.Lost:
                //todo show lose message
                //Debug.Log("Lost! no woooo");
                break;
        }
    }

    public void Reset()
    {
        timer = 0;
        state = GameState.PreGame;
        currentWaveIndex = 0;

        //reset base health
        foreach (Health baseHealth in baseHealths)
        {
            baseHealth.Reset();
        }
    }

    public float GetTimeLeft()
    {
        return timer;
    }

    public GameState GetState()
    {
        return state;
    }
    
    private bool AllEnemiesDead()
    {
        return true;
    }

    private bool AllPlayersReady()
    {
        return PlayerReady.AmountOfPlayersReady() >= PlayersNeededToStart;
    }

    [ClientRpc]
    private void RpcSendState(GameState state, float time)
    {
        this.state = state;
        this.timer = time;
    }

    [Command]
    private void CmdRequestCurrentState()
    {
        RpcSendState(state, timer);
    }
    //debug gui code
    public void OnGUI()
    {
        if (showDebugGUI)
        {
            //debug box in the top right
        GUI.Box(new Rect(Screen.width - 100, 0, 100, 20), "Timer: " + timer);
        GUI.Box(new Rect(Screen.width - 100, 20, 100, 20), "State: " + state);
        }
        
    }

    public void SendPlayerReady()
    {

    }
}
