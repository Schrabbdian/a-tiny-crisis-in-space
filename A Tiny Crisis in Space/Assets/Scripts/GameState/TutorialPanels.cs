﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPanels : MonoBehaviour
{
    [SerializeField] private GameStateManager gameStateManager;

    public List<RectTransform> tutorialPanels = new List<RectTransform>();
    public Button NextButton;
    public Button PreviousButton;
    public bool ShowTutorial = true;

    private int currentPanel = 0;
    private GameObject currentShownRect;
    private Text NextButtonText;
    private bool TutorialRead = false;

	// Use this for initialization
	void Start ()
	{
	    NextButtonText = NextButton.transform.GetChild(0).GetComponent<Text>();

	    if (!ShowTutorial || tutorialPanels.Count == 0)
	        TutorialRead = true;

        OnUIChanged();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnNextButton()
    {
        //show next panel
        if (currentPanel < tutorialPanels.Count - 1)
        {
            currentPanel++;
        }
        else
        {
            TutorialRead = true;
        }

        OnUIChanged();
    }

    public void OnPreviousButton()
    {
        //show previous panel
        if (currentPanel > 0)
        {
            currentPanel--;
        }
        
        OnUIChanged();
    }

    public void OnSkipButton()
    {
        //skip all tutorials and ready
        TutorialRead = true;

        OnUIChanged();
    }

    private void OnUIChanged()
    {

        if(currentShownRect != null)
            currentShownRect.gameObject.SetActive(false);

        if (currentPanel < tutorialPanels.Count)
        {
            currentShownRect = tutorialPanels[currentPanel].gameObject;

            if(currentShownRect != null)
                currentShownRect.gameObject.SetActive(true);
        }

        //show next button unless on last panel
        if (currentPanel < tutorialPanels.Count - 1)
        {
            NextButtonText.text = "Next";
        }
        else
        {
            NextButtonText.text = "Ready";
        }

        //show previous button unless on first panel
        if (currentPanel == 0)
        {
            PreviousButton.gameObject.SetActive(false);
        }
        else
        {
            PreviousButton.gameObject.SetActive(true);
        }

        if (TutorialRead)
        {
            this.gameObject.SetActive(false);
            PlayerReady.localInstance.CmdSetReady();
            FindObjectOfType<WinLosePanels>().m_LockCursor = true;
        }
    }
}
