﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLosePanels : MonoBehaviour
{
    public GameStateManager gameStateManager;
    public GameObject WinPanel;
    public GameObject LosePanel;
    public TutorialPanels Tutorial;
    public Text timerText;
    public bool m_LockCursor = true;

    private bool pressedEscape = false;

    // Use this for initialization
    void Awake()
    {
        if (Tutorial.ShowTutorial)
            m_LockCursor = false;

        // Lock or unlock the cursor.
        Cursor.lockState = m_LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !m_LockCursor;

        timerText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        switch (gameStateManager.GetState())
        {
            case GameStateManager.GameState.PreGame:
                break;

            case GameStateManager.GameState.PreWave:
                timerText.gameObject.SetActive(true);
                UpdateTimerText();
                break;

            case GameStateManager.GameState.Wave:
                UpdateTimerText();
                break;

            case GameStateManager.GameState.Won:

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    pressedEscape = true;
                }

                WinPanel.SetActive(!pressedEscape);
                timerText.gameObject.SetActive(false);

                break;

            case GameStateManager.GameState.Lost:

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    pressedEscape = true;
                }

                LosePanel.SetActive(!pressedEscape);
                timerText.gameObject.SetActive(false);
                break;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            m_LockCursor = !m_LockCursor;
        }
        
        if (m_LockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void UpdateTimerText()
    {
        TimeSpan t = TimeSpan.FromSeconds(gameStateManager.GetTimeLeft());

        string answer = string.Format("{0:D1}:{1:D2}",
            t.Minutes,
            t.Seconds);
        timerText.text = answer;
    }
}
