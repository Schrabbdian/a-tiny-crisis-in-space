﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public static Transform MainCameraTransform;

    public Health health;
    public bool RotateToFacePlayer;//uses main camera to face canvases
    public bool DisableOnLocalPlayer;
    public Slider UiSlider;
    public Text Text;


	// Use this for initialization
	protected virtual void Start ()
	{
	    MainCameraTransform = Camera.main.transform;
	    if (DisableOnLocalPlayer && health.isLocalPlayer)
	    {
	        gameObject.SetActive(false);
	    }
	}
	
	// Update is called once per frame
	protected virtual void Update ()
	{
        if(!health)
            return;

        //set text above head
        if(Text)
            Text.text = health.gameObject.name;

        //set slider value
        float value = Mathf.Clamp01(health.GetCurrentHealth() / health.GetMaxHealth());
	    UiSlider.value = value;

	    if (RotateToFacePlayer)
	    {
	        transform.LookAt(transform.position - MainCameraTransform.rotation * Vector3.back,
	            MainCameraTransform.rotation * Vector3.up);
	    }
	}
}
