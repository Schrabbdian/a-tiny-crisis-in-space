﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Cameras;

public class HUDHealthBar : HealthBar {

	// Use this for initialization
	protected override void Start () {
		base.Start();
        
	}
	
	// Update is called once per frame
	protected override void Update () {
	    //find player health
	    Transform cameraTarget = FindObjectOfType<FreeLookCam>().Target;
	    if (cameraTarget != null)
	    {
	        Health h = cameraTarget.gameObject.GetComponent<Health>();

	        if (h != null)
	        {
	            this.health = h;
	            UiSlider.enabled = true;
	        }

	        else
	        {
	            UiSlider.enabled = false;
	        }
	    }
	    else
	    {
	        UiSlider.enabled = false;
        }


        base.Update();
	}
}
