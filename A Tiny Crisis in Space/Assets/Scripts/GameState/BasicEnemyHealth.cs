﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using UnityEngine.AI;

public class BasicEnemyHealth : Health {
    [SerializeField] private GameObject deathParticle;

    protected override void OnZeroHealth()
    {
        RpcDeathAnimation();
    }

    [ClientRpc]
    public void RpcDeathAnimation()
    {
        StartCoroutine("dieing");
    }
    IEnumerator dieing()
    {
        this.GetComponent<EnemyAIScript>().enabled = false;
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<Animator>().enabled =false;
        for (int i = 0; i < 10; i++)
        {
            this.transform.localScale *= 0.7f;
            yield return new WaitForSeconds(0.02f);
        }

        Instantiate(deathParticle, transform.position + new Vector3(0, 0, 0), transform.rotation);
        Destroy(gameObject, 0.1f);
        Debug.Log(gameObject.name + " dead");
    }
}
