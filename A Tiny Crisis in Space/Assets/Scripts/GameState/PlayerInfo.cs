﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerIngameInfo {
    
    private string name;
    private readonly NetworkConnection connection;
    
    private GameObject playerObject;
    private Health health;
    private MergeController mergeController;
    
    public PlayerIngameInfo(
        string name, 
        NetworkConnection connection, 
        GameObject playerObject, 
        Health health, 
        MergeController mergeController)
    {
        this.name = name;
        this.connection = connection;
        this.playerObject = playerObject;
        this.health = health;
        this.mergeController = mergeController;
    }

    public GameObject PlayerObject
    {
        get { return playerObject; }
        set { playerObject = value; }
    }

    public Health Health
    {
        get { return health; }
        set { health = value; }
    }

    public MergeController MergeController
    {
        get { return mergeController; }
        set { mergeController = value; }
    }

    public NetworkConnection Connection
    {
        get { return connection; }
    }

    public string Name
    {
        get { return name; }
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        if (!(obj is PlayerIngameInfo))
            return false;

        PlayerIngameInfo info = (PlayerIngameInfo) obj;

        if (info == this)
            return true;

        if (info.connection == null)
            return false;

        return info.connection.Equals(this.connection);
    }

    public override int GetHashCode()
    {
        return connection.GetHashCode();
    }
}
