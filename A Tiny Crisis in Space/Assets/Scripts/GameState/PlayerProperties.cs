﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerProperties : NetworkBehaviour
{
    [SyncVar(hook = "OnSetID")] public int ID = -1;
    [SyncVar(hook = "OnSetName")] public string playerName;

    [Header("Materials")]
    [SerializeField] private Renderer Model;

    [SerializeField] private bool ReplaceTwoMaterials;
    public Material[] Materials0;
    public Material[] Materials1;

    // Use this for initialization
    void Start () {
        OnSetID(ID);
        OnSetName(playerName);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnSetID(int value)
    {
        ID = value;
        //change materials if needed
        if (Model)
        {
            if (value < 0 || value > 3)
            {
                Debug.LogError("Undefined player ID set : " + value + ". Using default value 0");
                value = 0;
            }

            Material[] newMats;
            if (ReplaceTwoMaterials)
            {
                newMats = new[]{Materials0[value], Materials1[value]};
            }
            else
            {
                newMats = new[] { Materials0[value]};
            }

            Model.materials = newMats;
        }
    }

    private void OnSetName(string value)
    {
        playerName = value;
        gameObject.name = value;
    }
}
