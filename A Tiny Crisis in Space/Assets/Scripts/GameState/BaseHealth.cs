﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class BaseHealth : Health {

    [Header("Screen Shake On Damage")]
    [SerializeField]
    private float ShakeDuration = 0.4f;
    [SerializeField]
    private float ShakeAmount = 25.0f;

    [Header("Damage Visual Effect")]
    [SerializeField]
    private Color blinkColor;

    private FreeLookCam playerCam;
    private DamageEffect dmgEffect;

    protected override void OnZeroHealth()
    {
        Debug.Log(gameObject.name + " dead");
    }

    protected override void OnHealthChanged(float changedHealth)
    {
        base.OnHealthChanged(changedHealth);

        //if health is being reduced
        if (!IsDead() && (currentHealth - changedHealth) > 1.0f)
        {
            //lazy initialize component references
            if (playerCam == null)
            { playerCam = Camera.main.transform.parent.parent.GetComponent<FreeLookCam>(); }
            
            if (dmgEffect == null)
            { dmgEffect = GameObject.FindObjectOfType<DamageEffect>(); }


            //if references set up
            if (playerCam != null)
            {
                //activate screen shake
                playerCam.ScreenShake(ShakeAmount, ShakeDuration);
            }

            if (dmgEffect != null)
            {
                //activate damage overlay blinking
                dmgEffect.StartBlink(blinkColor);
            }
        }
    }
}
