﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour
{
    

    public enum DamageType
    {
        PlayerProjectile, PlayerBomb, Enemy, World
    }
    // Color for blinking if the Object is hit
    [SerializeField] private Color col =Color.red;
    // The color the objet had at the start
    private Color baseColor;
    // The Corotine started last
    private IEnumerator lastBlink;
    private Renderer rend;
    [SerializeField] private float blinkFrames = 30;

    [SerializeField]
    private float MaxHealth = 100;

    public bool Regenerate = false;
    [SerializeField] private float RegenerationPerSecond = 0;
    [SerializeField] private float RegenerationDelay = 3;
    private float lastDamage = -10;//regenerate from the start

    [SyncVar(hook = "HealthHook")] protected float currentHealth = 1;//spawn with 1 health so not counted as dead


    protected NetworkInstanceId id;
    private bool initialized = false;

    [Header("Debug")]
    public bool DrawDebugHealthBar; 

	// Use this for initialization
	public override void OnStartServer ()
	{
        Initialize(MaxHealth);
    }

    private void Start()
    {

        rend = GetComponentInChildren<Renderer>();
        id = GetComponent<NetworkIdentity>().netId;
        baseColor = rend.material.color;
    }

    private void Initialize(float healthAmount)
    {
        if (!initialized)
        {
            currentHealth = healthAmount;
            initialized = true;
        }
    }
	
	// Update is called once per frame
	protected virtual void Update () {

	    if (isServer && CanRegen())
	    {
	        Regen();
	    }
	}

    public void Reset()
    {
        if(isServer)
            currentHealth = MaxHealth;
        else
        {
            Debug.Log("cannot reset health on a client");
        }
    }

    //only used after certain state changes like merging and demerging
    public void SetStartHealth(float amount)
    {
        Initialize(amount);
    }

    public virtual void TakeDamage(float damage, DamageType type)
    {
        if (!isServer)
        {
            CmdTakeDamage(id, damage, type);
        }
        else
        {
            lastDamage = Time.time;

            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                currentHealth = 0;

                OnZeroHealth();
            }
        }
    }

    IEnumerator Blink()
    {
        Color change = (baseColor-col)/blinkFrames*2;
        for (int i = 0; i < blinkFrames; i++)
        {
            if (i < blinkFrames / 2)
            {
                rend.material.color -= change;
            }
            else
            {
                rend.material.color += change;
            }
            yield return null;
        }
        rend.material.color = baseColor;
    }

    [Command]
    private void CmdTakeDamage(NetworkInstanceId ID, float damage, DamageType type)
    {
        //find player in scene and apply damage
        GameObject localObject = NetworkServer.FindLocalObject(ID);
        if (localObject != null)
        {
            Health p = localObject.GetComponent<Health>();
            p.TakeDamage(damage, type);
        }
    }

    protected virtual void OnZeroHealth()
    {
        Destroy(gameObject);
        Debug.Log(gameObject.name + " dead");
    }


    public virtual float GetCurrentHealth()
    {
        if(isServer)
            Initialize(MaxHealth);
            
        return currentHealth;
    }

    private void HealthHook(float changedHealth)
    {
        OnHealthChanged(changedHealth);
        if (!isServer) { currentHealth = changedHealth; }
    }

    protected virtual void OnHealthChanged(float changedHealth)
    {
        if ((currentHealth - changedHealth) > 1.0f && !IsDead())
        {
            // Blinks if hit
            if (lastBlink != null)
            {
                StopCoroutine(lastBlink);
            }
            lastBlink = Blink();
            StartCoroutine(lastBlink);
        }
    }

    public float GetMaxHealth()
    {
        return MaxHealth;
    }

    public bool IsDead()
    {
        return GetCurrentHealth() <= 0;
    }

    //has ability to regenerate, isnt dead and hsnt taken damage recently
    private bool CanRegen()
    {
        return Regenerate && !IsDead() && Time.time >= lastDamage + RegenerationDelay;
    }

    private void Regen()
    {
        float newHealth = currentHealth + RegenerationPerSecond * Time.deltaTime;
        if (newHealth > MaxHealth)
        {
            newHealth = MaxHealth;
        }

        currentHealth = newHealth;
    }

    //healthbar code https://docs.unity3d.com/Manual/UNetSetup.html
    private GUIStyle healthStyle;
    private GUIStyle backStyle;

    private void OnGUI()
    {
        InitStyles();

        // Draw a Health Bar
        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        if(pos.z < 0)
            return;

        // draw health bar background
        GUI.color = DrawDebugHealthBar ? Color.red : new Color(0,0,0,0);
        GUI.backgroundColor = DrawDebugHealthBar ? Color.red : new Color(0, 0, 0, 0);
        GUI.Box(new Rect(pos.x - 26, Screen.height - pos.y + 20, MaxHealth / 2, 7), ".", backStyle);

        // draw health bar amount
        if (!IsDead())
        {
            GUI.color = DrawDebugHealthBar ? Color.green : new Color(0, 0, 0, 0);
            GUI.backgroundColor = DrawDebugHealthBar ? Color.green : new Color(0, 0, 0, 0);
            GUI.Box(new Rect(pos.x - 25, Screen.height - pos.y + 21, GetCurrentHealth() / 2, 5), ".", healthStyle);
        }
        
    }

    private void InitStyles()
    {
        if (healthStyle == null)
        {
            healthStyle = new GUIStyle(GUI.skin.box);
            healthStyle.normal.background = MakeTex(2, 2, new Color(0f, 1f, 0f, 1.0f));
        }

        if (backStyle == null)
        {
            backStyle = new GUIStyle(GUI.skin.box);
            backStyle.normal.background = MakeTex(2, 2, new Color(1f, 0f, 0f, 1.0f));
        }
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

}
