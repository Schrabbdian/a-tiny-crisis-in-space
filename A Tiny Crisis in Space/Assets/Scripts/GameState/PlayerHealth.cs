﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Cameras;

public class PlayerHealth : Health
{
    public string ReviveButton;
    public float ReviveRange;
    public int PlayerRecoveryHealth = 50;
    public LayerMask ReviveLayerMask;//only hit players and walls

    [Header("Damage Visual Effect")]
    [SerializeField]
    private Color blinkColor;
    private DamageEffect dmgEffect;

    protected override void Update()
    {
        base.Update();
        if (!isLocalPlayer)
        {
            return;
        }


        if (Input.GetButtonDown(ReviveButton))
        {
            CheckRevive();   
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(10f, DamageType.World);
        }
    }

    private void CheckRevive()
    {
        Camera mainCam = Camera.main;
    
        //raycast from camera to find player
        Ray reviveRay = new Ray(mainCam.transform.position, mainCam.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(reviveRay, out hit, 100f, ReviveLayerMask))
        {
            //check for player
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                //check for health component that is not your own and 0 health
                PlayerHealth p = hit.collider.gameObject.GetComponent<PlayerHealth>();
                if (p && p != this && p.IsDead())
                {
                    //check range
                    float distance = Vector3.Distance(hit.collider.transform.position, transform.position);
                    if (distance <= ReviveRange)
                    {
                        ReviveOther(p);
                    }
                }
            }
        }
    }

    public void ReviveOther(PlayerHealth player)
    {
        if (!isServer)
        {
            CmdRevive(player.id);
        }
        else
        {
            player.Revive();
        }
    }

    private void Revive()
    {
        currentHealth = PlayerRecoveryHealth;
    }

    [Command]
    private void CmdRevive(NetworkInstanceId ID)
    {
        //find player in scene and apply damage
        GameObject player = NetworkServer.FindLocalObject(ID);
        PlayerHealth p = player.GetComponent<PlayerHealth>();
        p.Revive();
    }

    protected override void OnZeroHealth()
    {
        Debug.Log("Player " + gameObject.name + " needs to be revived!");
    }

    protected override void OnHealthChanged(float changedHealth)
    {
        base.OnHealthChanged(changedHealth);

        if (!isLocalPlayer) return;

        //if health is being reduced
        if (!IsDead() && (currentHealth - changedHealth) > 1.0f)
        {
            //lazy initialize component references
            if (dmgEffect == null)
            { dmgEffect = GameObject.FindObjectOfType<DamageEffect>(); }


            //if references set up
            if (dmgEffect != null)
            {
                //activate damage overlay blinking
                dmgEffect.StartBlink(blinkColor);
            }
        }
    }
}
