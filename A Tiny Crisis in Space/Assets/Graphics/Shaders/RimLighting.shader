﻿Shader "Custom/RimLighting" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_NormalMap("Normalmap", 2D) = "bump" {}
		_NormalStrength("Normal Map Strength", Range(-1, 1)) = 1.0

		_RimColor("Rim Color", Color) = (0.26, 0.19, 0.16, 0.0)
		_RimPower("Rim Power", Range(0.5, 8.0)) = 3.0
		_Emission("Emission Color", Color) = (0, 0, 0, 1)
		_EmissionTex("Emission Map", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		//Texture samplers
		sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _EmissionTex;

		//Scalars & Colors
		half _Glossiness;
		half _Metallic;
		half _RimPower;
		half _NormalStrength;
		fixed4 _Color;
		fixed4 _RimColor;
		fixed4 _Emission;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
			float3 viewDir;
		};


		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));

			//scale normal by strength value
			o.Normal.xy *= _NormalStrength;
			o.Normal = normalize(o.Normal);

			//add rim lighting to the emission proprty of the material
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			fixed4 emission = tex2D(_EmissionTex, IN.uv_MainTex);
			o.Emission = _RimColor.rgb * pow(rim, _RimPower) + _Emission * emission;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
