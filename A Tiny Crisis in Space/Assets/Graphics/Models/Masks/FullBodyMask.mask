%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: FullBodyMask
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Astronaut
    m_Weight: 1
  - m_Path: Astronaut_Rig
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/arm_L
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/arm_L/forearm_L
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/arm_L/forearm_L/hand_L
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/arm_R
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/arm_R/forearm_R
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/arm_R/forearm_R/hand_R
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/neck
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/spine/chest/neck/head
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_L
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_L/leg_L
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_L/leg_L/foot_L
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_L/leg_L/foot_L/toes_L
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_R
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_R/leg_R
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_R/leg_R/foot_R
    m_Weight: 1
  - m_Path: Astronaut_Rig/Hips/upperleg_R/leg_R/foot_R/toes_R
    m_Weight: 1
