%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: MergeTop_RightArm
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Merge_top
    m_Weight: 0
  - m_Path: MergeTop_Armature
    m_Weight: 0
  - m_Path: MergeTop_Armature/Master
    m_Weight: 0
  - m_Path: MergeTop_Armature/Master/Body
    m_Weight: 0
  - m_Path: MergeTop_Armature/Master/Body/arm_L
    m_Weight: 0
  - m_Path: MergeTop_Armature/Master/Body/arm_L/forearm_L
    m_Weight: 0
  - m_Path: MergeTop_Armature/Master/Body/arm_L/forearm_L/hand_L
    m_Weight: 0
  - m_Path: MergeTop_Armature/Master/Body/arm_R
    m_Weight: 1
  - m_Path: MergeTop_Armature/Master/Body/arm_R/forearm_R
    m_Weight: 1
  - m_Path: MergeTop_Armature/Master/Body/arm_R/forearm_R/hand_R
    m_Weight: 1
  - m_Path: MergeTop_Armature/Master/Body/Head
    m_Weight: 0
  - m_Path: MergeTop_Armature/Master/Head_StretchTarget
    m_Weight: 0
