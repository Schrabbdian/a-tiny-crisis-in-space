%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: RoballMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Roball
    m_Weight: 1
  - m_Path: Roball_Armature
    m_Weight: 1
  - m_Path: Roball_Armature/Body
    m_Weight: 1
  - m_Path: Roball_Armature/Body/backButton
    m_Weight: 1
  - m_Path: Roball_Armature/Body/backLeg_base_L
    m_Weight: 1
  - m_Path: Roball_Armature/Body/backLeg_base_L/backLeg_L
    m_Weight: 1
  - m_Path: Roball_Armature/Body/backLeg_base_L/backLeg_L/backClaw_L
    m_Weight: 1
  - m_Path: Roball_Armature/Body/backLeg_base_R
    m_Weight: 1
  - m_Path: Roball_Armature/Body/backLeg_base_R/backLeg_R
    m_Weight: 1
  - m_Path: Roball_Armature/Body/backLeg_base_R/backLeg_R/backClaw_R
    m_Weight: 1
  - m_Path: Roball_Armature/Body/frontButton
    m_Weight: 1
  - m_Path: Roball_Armature/Body/frontLeg_base_L
    m_Weight: 1
  - m_Path: Roball_Armature/Body/frontLeg_base_L/frontLeg_L
    m_Weight: 1
  - m_Path: Roball_Armature/Body/frontLeg_base_L/frontLeg_L/frontClaw_L
    m_Weight: 1
  - m_Path: Roball_Armature/Body/frontLeg_base_R
    m_Weight: 1
  - m_Path: Roball_Armature/Body/frontLeg_base_R/frontLeg_R
    m_Weight: 1
  - m_Path: Roball_Armature/Body/frontLeg_base_R/frontLeg_R/frontClaw_R
    m_Weight: 1
  - m_Path: Roball_Armature/Body/GunBase
    m_Weight: 0
  - m_Path: Roball_Armature/Body/GunBase/GunPart01
    m_Weight: 1
  - m_Path: Roball_Armature/Body/GunBase/GunPart01/GunPart02
    m_Weight: 1
  - m_Path: Roball_Armature/Body/GunBase/GunPart01/GunPart02/GunPart03
    m_Weight: 1
