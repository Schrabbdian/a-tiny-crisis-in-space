Character:

- Movement while shooting:
- When shooting, the character turns to face the shooting direction (= camera view direction)
- When NOT shooting, character faces (last) walking direction

- Consideration: Why not always shoot?
- Option A: Movement is impaired while shooting
--> No slow-down while shooting = player very powerful. might lead to enemies not being a significant threat (can just run away while shooting)

- Option B: Ammo System:
--> Overheating / Recharge ammo
--> Ammo Pickups
--> Friendly fire helpful? Feels a bit tacked on

- Option C: Overheat / Recharge + Slowed Movement

- Can revive downed players if standing beside them (cannot shoot while reviving)
- Downed players can move very slowly
- Should downed players die until next wave if not revived within time limit?
- Health Regen


Enemies:

- Fodder enemies
(can be killed with any weapon / alone / together)
--> melee
--> Nice to have: Multiple varieties (possibly cheese this through scale / recolor & health increase)

- "Special" Enemies
--> force players to merge or unmerge
--> ranged weapons

- Enemy Damage: Visualized through flashing
- No Damage: No flashing + different SFX

- Enemies walk towards the nearest objective (or an objective defined per enemy spawn point)


Spawn Manager:

- Enemies Spawn in Predefined Groups (e.g. 10 fodder + 2 special)
- Predefined Spawn Points
- System which spawns enemies in areas where there aren't many
- Wave based (e.g. Wave duration 5 min, spawn 7 groups)


Communication:
- Some way to get the attention of others / call them to you
- Nice to Have: "Ping" System



Merges:

- merging is possible at any time, requires 2 players to be within a radius
- 1 player presses merge button to "offer", other player "accepts"
- players need to coordinate to control the merged state
- some sort of cooldown to avoid spamming the merge
- cannot carry while merged / merge while carrying something


Objectives:

- Protect Certain Objects
- there are 2 such objectives in our map
- Enemies can focus both players and objective

- Secondary Objective:
--> Carry enemy drop collectibles towards defended objective
--> Idea: Make it so these collectibles should always be carried towards the OPPOSITE objective (makes players cross map)

- Nice to have: Track who did what (e.g. enemies killed) to display after game ends
