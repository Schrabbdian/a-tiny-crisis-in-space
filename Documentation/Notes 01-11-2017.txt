Just some thoughts on the
Physical Prototype:

- make it "turn-based" (only for the prototype)
- players can use their turn to move or defeat enemies
--> if they are in adjacent spaces they can also merge
--> a- un-merged player can defeat, say, 1 enemy a trun while merged players can defeat more (e.g. 5)
--> some enemies can ONLY be defeated in merged state or while on opposite sides of the enemy

- tradeoff between moving / positioning and defeating enemies

- enemies have their own turn where they move either to the closest player or toward the closest defense zone

Thoughts on various Parts of Game Proposal:

"Big Idea Bullseye":
- Big Idea: Coop Third Person Shooter against Waves of AI enemies
- Secondary: Network-based Multiplayer, Merging into one jointly controlled Form and back into individual Player Characters, Animation Blending and Synching

